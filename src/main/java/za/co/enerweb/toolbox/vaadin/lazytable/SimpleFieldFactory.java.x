package za.co.enerweb.toolbox.vaadin.lazytable;

import java.util.Date;

import lombok.Setter;
import za.co.enerweb.toolbox.reflection.PropertyUtils;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.PopupDateField;

public class SimpleFieldFactory extends DefaultFieldFactory {
    private static final String DATE_FORMAT = // "yyyy-MM-dd";
        "yyyy-MM-dd HH:mm:ss"; // "yyyy-MM-dd HH:mm:ss.S"

    private boolean first = true;
    @Setter
    private boolean editable = false;

    public SimpleFieldFactory(boolean editable) {
        this.editable = editable;
    }

    /**
     * XXX: rather figure out how to use renderers here?!
     * As far as I can tell we will need an environment per item,
     * which may be too much overhead. Maybe we can redo renderers
     * to allow for non env things i.e. use a Propery-like interface
     * instead of env directly. But I think some renderers need an envId
     * so maybe in this case we don't support them.
     */
    @Override
    public Field createField(Container container, Object itemId,
        Object propertyId, Component uiContext) {
        Field ret = _createField(container, itemId,
            propertyId, uiContext);
        ret.setReadOnly(!editable);
        return ret;
    }

    public Field _createField(Container container, Object itemId,
        Object propertyId, Component uiContext) {

        Item item = container.getItem(itemId);
        Object bean = LazyQuery.fromItem(item);
        Object value = PropertyUtils.getProperty(bean, (String) propertyId);
        Class<?> type =
            PropertyUtils.getPropertyDescriptor(bean, (String) propertyId)
                .getPropertyType();

        if (type.equals(Date.class)) {
            PopupDateField field = new PopupDateField(null, (Date) value);
            field.setResolution(PopupDateField.RESOLUTION_SEC);
            field.setDateFormat(DATE_FORMAT);
            field.setLenient(true);
            if (first) {
                field.focus();
                first = false;
            }
            return field;
        }

        // if (type.equals(type.SCALAR)) {
        // TextField field = new TextField();
        // field.setReadOnly(!table.isEditable());
        // if (dataType.equals(
        // DataType.NUMBER)) {
        // // align right
        // field.addStyleName("ebr-numeric-text-field");
        // }
        // field.setValue("" + value);
        // return field;
        // } else if (type.equals(type.ENUM)) {
        // NativeSelect field = new NativeSelect();
        // field.setNullSelectionAllowed(false);
        // for (CmEnum e : cmParamdef.getCmDatatype()
        // .getCmEnumCollection()) {
        // field.addItem(e.getKey());
        // field.setItemCaption(e.getKey(), e.getCaption());
        // if (e.getKey().equals(value)) {
        // field.select(e.getKey());
        // }
        // }
        // field.setValue(value);
        // return field;
        // }
        // }

        // Otherwise use the default field factory
        return super.createField(container, itemId, propertyId,
            uiContext);
    }
}
