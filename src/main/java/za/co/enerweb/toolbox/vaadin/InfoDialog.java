package za.co.enerweb.toolbox.vaadin;

import lombok.Getter;
import lombok.Setter;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;

/**
 * Useful for windows that you want to close by hitting <Enter> or <Escape>
 * I basically just contains a close button by default;
 * XXX: rename to InfoDialog
 */
@SuppressWarnings("serial")
public abstract class InfoDialog extends Dialog {

    private static final long serialVersionUID = 1L;

    @Getter @Setter
    private boolean dontSteelFocus;

    private Button closeButton;

    {
        closeButton = new Button("Close", new ClickListener() {
            @Override
            public void buttonClick(final ClickEvent event) {
                close();
            }
        });
    }

    public InfoDialog() {
    }

    /**
     * @deprecated use setDontSteelFocus()
     */
    @Deprecated
    public InfoDialog(final boolean focusOnOkButton) {
        this();
        dontSteelFocus = !focusOnOkButton;
    }

    public InfoDialog(final String caption) {
        this();
        setCaption(caption);
    }

    public void attach() {
        super.attach();

        getDialogLayout().addStyleName("ebr-confirmation-dialog");

        // get the focus on the window so that Enter or Escape,
        // closes it.
        if (!dontSteelFocus) {
            closeButton.focus();
        }
    }

//    /*
//     * (non-Javadoc)
//     * @see com.vaadin.event.Action.Handler#getActions(java.lang.Object,
//     * java.lang.Object)
//     */
//    @Override
//    public Action[] getActions(final Object target, final Object sender) {
//        return new Action[] {actionEnter, actionEscape};
//    }
//
//    /*
//     * (non-Javadoc)
//     * @see
//     * com.vaadin.event.Action.Handler#handleAction(com.vaadin.event.Action,
//     * java.lang.Object, java.lang.Object)
//     */
//    @Override
//    public void handleAction(final Action action, final Object sender,
//        final Object target) {
//        if (action == actionEnter || action == actionEscape) {
//            close();
//        }
//    }

    /**
     * the component will be added to the window
     * @return
     */
    protected final Component getDialogContent() {
        addButton(closeButton);
        return getInfoDialogContent();
    }

    /**
     * the component will be added to the window
     * @return
     */
    protected abstract Component getInfoDialogContent();

}
