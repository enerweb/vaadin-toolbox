package za.co.enerweb.toolbox.vaadin;

import za.co.enerweb.toolbox.vaadin.ConfirmationDialog.ConfirmationListener;

import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;

public abstract class AEditorDialog extends
    ConfirmationDialog
    implements ValueChangeListener, ConfirmationListener {

    private GridLayout layout;

    public AEditorDialog(String caption) {
        this();
        setCaption(caption);
    }

    public AEditorDialog() {
        setWidth("540px");
        setHeight("450px");
        // setScrollable(true); // vaadin7
        setOkButtonCaption("Save");
        addConfirmationListener(this);
    }

    @Override
    protected final Component getConfirmationDialogContent() {
        reload();
        return layout;
    }

    public void reload() {
        layout = new GridLayout(2, 2);
        layout.setSizeFull();
        layout.setSpacing(true);
        populateLayout(layout);
        Label spacer = new Label();
        layout.addComponent(spacer);
        layout.setRowExpandRatio(layout.getRows() - 1, 100f);
    }

    protected abstract void populateLayout(GridLayout layout);

    protected void addLabel(String text) {
        layout.addComponent(new Label(text + " :"));
    }

    @Override
    public void onCancel() {
    }
}
