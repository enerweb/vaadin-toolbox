package za.co.enerweb.toolbox.vaadin;

import static lombok.AccessLevel.NONE;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import lombok.Getter;
import lombok.Setter;

import com.vaadin.event.Action;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;

/**
 * Useful confirmation dialog that you want to close by hitting <Enter> or
 * <Escape>
 * By default this dialog is unsized, but if you set the size
 * then the content should fill it.
 * Usage example:
 * ConfirmationDialog cd = new ConfirmationDialog(
 * "This is a dead end.",
 * "Do you want to continue?");
 * cd.addConfirmationListener(new ConfirmationListener(){...});
 * cd.show(someAttachedComponent);
 * Subclasses can also override getConfirmationDialogContent()
 * (Other customizations can also be done here)
 */
@Getter
@Setter
public class ConfirmationDialog extends Dialog {

    private static final long serialVersionUID = 1L;

    @Getter(NONE)
    @Setter(NONE)
    private Collection<ConfirmationListener> listeners =
        new ArrayList<ConfirmationListener>();

    @Getter(NONE)
    private Component dialogContent = new Heading("Are you sure?");

    @Getter(NONE)
    @Setter(NONE)
    private boolean proceedWithClose;

    private boolean dontSteelFocus;

    private String okButtonCaption = "Ok";

    private String cancelButtonCaption = "Cancel";

    private Button okButton;
    private Button cancelButton;

    private boolean showOkButton = true;

    private boolean showCancelButton = true;

    @Getter(NONE)
    @Setter(NONE)
    private boolean populated = false;

    {
        // common init
        setModal(true);
        okButton = getButtons().createButton(okButtonCaption, "",
            KeyCode.O, "O", null);

        cancelButton = getButtons().createButton(cancelButtonCaption, "",
            KeyCode.C, "C", null);

    }

    /**
     * see also AbstractConfirmationListener or OkListener
     */
    // XXX: maybe these methods must be able to return a boolean
    // Indicating whether the window must still be closed
    // so that implementors don't need a reference to the dialog
    // for that functionality.
    // XXX: It should also include context as to the source
    // But then we need to make a new interface :(
    // so that we don't loose backwards compatibility
    public static interface ConfirmationListener {
        void onOk();

        void onCancel();
    }

    public static class AbstractConfirmationListener implements
        ConfirmationListener, Serializable {
        private static final long serialVersionUID = 1L;

        public void onOk() {
        }

        public void onCancel() {
        }
    }

    public abstract static class OkListener implements
        ConfirmationListener, Serializable {
        private static final long serialVersionUID = 1L;

        public final void onCancel() {
        }
    }

    public ConfirmationDialog() {
        setCaption("Confirmation dialog.");
    }

    public ConfirmationDialog(final String caption) {
        setCaption(caption);
    }

    public ConfirmationDialog(final String caption, final String content) {
        setCaption(caption);
        dialogContent = new Heading(content);
        // dialogContent.setSizeFull();
    }

    public ConfirmationDialog(final String caption, final Component content) {
        this(caption);
        dialogContent = content;
    }

    /**
     * give ok and cancel actions the chance to say they do not
     * want the dialog to close now
     */
    public void cancelClose() {
        proceedWithClose = false;
    }

    /*
     * (non-Javadoc)
     * @see
     * com.vaadin.event.Action.Handler#handleAction(com.vaadin.event.Action,
     * java.lang.Object, java.lang.Object)
     */
    @Override
    public void handleAction(final Action action, final Object sender,
        final Object target) {
        if (action == getActionEnter() && isCloseOnEnter()) {
            okAction();
        }
        if (action == getActionEscape() && isCloseOnEscape()) {
            cancelAction();
        }
    }

    private void cancelAction() {
        proceedWithClose = true;
        fireOnCancel();
        if (proceedWithClose) {
            reallyClose();
        }
    }

    private void okAction() {
        proceedWithClose = true;
        fireOnOk();
        if (proceedWithClose) {
            reallyClose();
        }
    }

    public void reallyClose() {
        super.close();
    }

    public void close() {
        cancelAction();
    }

    private void fireOnOk() {
        for (ConfirmationListener listener : listeners) {
            listener.onOk();
        }
    }

    private void fireOnCancel() {
        for (ConfirmationListener listener : listeners) {
            listener.onCancel();
        }
    }

    public void addConfirmationListener(final ConfirmationListener listener) {
        listeners.add(listener);
    }

    public void attach() {
        super.attach();
        getDialogLayout().addStyleName("ebr-confirmation-dialog");

        // get the focus on the window so that Enter or Escape,
        // closes it.
        if (!dontSteelFocus) {
            okButton.focus();
        }
    }

    @Override
    public final Component getDialogContent() {
        if (!populated) {
            dialogContent = getConfirmationDialogContent(); // let subclasses
                                                            // decide what to
                                                            // show here
            if(showOkButton) {
                // for in case it changed
                okButton.setCaption(okButtonCaption);
                addButton(okButton);
            }
            if(showCancelButton){
                cancelButton.setCaption(cancelButtonCaption);
                addButton(cancelButton);
            }
            populated = true;
        }
        return dialogContent;
    }

    /**
     * Override this to customise the content.
     */
    protected Component getConfirmationDialogContent() {
        return dialogContent;
    }

    @Override
    public void buttonClick(ClickEvent event) {
        if (event.getSource().equals(okButton)) {
            okAction();
        } else if (event.getSource().equals(cancelButton)) {
            cancelAction();
        }
    }

    public void dontSteelFocus() {
        setDontSteelFocus(true);
    }
}
