package za.co.enerweb.toolbox.vaadin.lazytable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import lombok.val;
import lombok.extern.slf4j.Slf4j;

import org.vaadin.addons.lazyquerycontainer.CompositeItem;
import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;

import za.co.enerweb.toolbox.throwable.ThrowableUtils;
import za.co.enerweb.toolbox.vaadin.NotificationUtil;
import za.co.enerweb.toolbox.vaadin.lazytable.filter.AFilter;

import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;

@Slf4j
@SuppressWarnings("serial")
public class LazyQuery implements Query, Serializable {

    private static final String DATA_OBJECT = "DATA_OBJECT";
    private transient ALazyDataProducer iDataProducer;
    private List<PropertyInfo> propertyInfos;
    private Class<?> beanClass;
    private Map<Object, String> colDisplayProperties;
    private QueryDefinition definition;
    private SortSpec sortSpec;
    private final AFilter aFilter;
    private Integer count = null;
    private final LazyTable<?> lazyTable;

    public LazyQuery(
        ALazyDataProducer iDataProducer,
        List<PropertyInfo> propertyInfos,
        Class<?> entityClass, Map<Object, String> colDisplayProperties,
        final QueryDefinition definition,
        final SortSpec sortSpec, AFilter aFilter,
        LazyTable<?> lazyTable) {
        this.iDataProducer = iDataProducer;
        this.propertyInfos = propertyInfos;
        this.beanClass = entityClass;
        this.colDisplayProperties = colDisplayProperties;
        this.definition = definition;
        this.sortSpec = sortSpec;
        this.aFilter = aFilter;
        this.lazyTable = lazyTable;

    }

    @Override
    public Item constructItem() {
        return toItem((Serializable) iDataProducer.newInstance());
    }

    @Override
    public int size() {
        if (count == null) {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            Future<Integer> future = executor.submit(new Callable<Integer>() {

                @Override
                public Integer call() throws Exception {

                    try {
                        return iDataProducer.count(aFilter);
                    } catch (Exception e) {
                        log.debug("Could not lazy load data.", e);
                        new NotificationUtil(lazyTable).btw(
                            "Could not lazy load data. ");
                    }
                    return 0;
                }
            });

            try {
                count = future.get(5, TimeUnit.SECONDS);
            } catch (Exception e) {
                log.warn("Could not determine lazy data size.", e);

                new NotificationUtil(lazyTable).btw(
                    "Could not determine data size. "
                        + ThrowableUtils.getMessagesAsString(e));
                count = 0;
                executor.shutdownNow();
            }
        }
        return count;
    }

//    @AllArgsConstructor
//    public static class EntityWrapper implements Serializable {
//        @Getter
//        private Serializable entity;
//    }

    @Override
    public List<Item> loadItems(final int startIndex, final int count) {
        List<?> beans = iDataProducer
            .find(startIndex, count, sortSpec, aFilter);

        List<Item> items = new ArrayList<Item>();
        for (Object entity : beans) {
            items.add(toItem((Serializable) entity));
        }

        return items;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void saveItems(final List<Item> addedItems,
        final List<Item> modifiedItems,
        final List<Item> removedItems) {
        iDataProducer.saveItems(fromItemList(addedItems),
            fromItemList(modifiedItems), fromItemList(removedItems));
    }

    @Override
    public boolean deleteAllItems() {
        throw new UnsupportedOperationException();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private Item toItem(final Serializable dataObject) {
        LazyDataItem<Serializable> entityItem =
            new LazyDataItem<Serializable>(dataObject,
                iDataProducer, propertyInfos,
                colDisplayProperties, false);

        CompositeItem compositeItem = new CompositeItem();
        compositeItem.addItem(DATA_OBJECT, entityItem);

        for (Object propertyId : definition.getPropertyIds()) {
            if (compositeItem.getItemProperty(propertyId) == null) {
                compositeItem.addItemProperty(propertyId, new ObjectProperty(
                    definition.getPropertyDefaultValue(propertyId),
                    definition.getPropertyType(propertyId),
                    definition.isPropertyReadOnly(propertyId)));
            }
        }

        return compositeItem;
    }

    public static Serializable fromItem(final Item item) {
        return dataItemfromItem(item).getDataObject();
    }

    public static List<Serializable> fromItemList(final List<Item> items) {
        val ret = new ArrayList<Serializable>(items.size());
        for (Item item : items) {
            ret.add(fromItem(item));
        }
        return ret;
    }

    @SuppressWarnings("unchecked")
    public static LazyDataItem<Serializable> dataItemfromItem(
        final Item item) {
        return (LazyDataItem<Serializable>) ((CompositeItem) item).getItem(
            DATA_OBJECT);
    }
}
