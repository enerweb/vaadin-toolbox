package za.co.enerweb.toolbox.vaadin.lazytable;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@Data
// @AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(chain = true, fluent = true)
@EqualsAndHashCode(of = "id")
public class PropertyInfo implements Serializable {
    /**
     * name of the field if it is a bean,
     * but can be an object index too
     */
    Serializable id;
    String caption = null;
    Class<?> type = null;
    boolean readonly = false;

    // IValueGenerator generator = null;

    public PropertyInfo(String fieldName) {
        this.id = fieldName;
    }

    public PropertyInfo(String fieldName, String caption) {
        this.id = fieldName;
        this.caption = caption;
    }

    public PropertyInfo(Serializable id, String caption, Class<?> type) {
        this.id = id;
        this.caption = caption;
        this.type = type;
    }

    public String toString() {
        return id.toString();
    }

    // public boolean isGenerated() {
    // return generator != null;
    // }

    // public String getFieldName() {
    // return (String) id;
    // }
}
