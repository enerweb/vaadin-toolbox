package za.co.enerweb.toolbox.vaadin;

import java.io.IOException;
import java.io.InputStream;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import za.co.enerweb.toolbox.io.ResourceUtils;

import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;

@Slf4j
public abstract class StreamResourceUtil {

    /**
     * @param resourcePath eg. "za/co/enerweb/ebr/mdb/icons/table_edit.png"
     */
    public static StreamResource getFromClasspath(
        final String resourcePath) {
        String filename = FilenameUtils.getName(resourcePath);
        StreamSource streamSource = new StreamSource() {

            @Override
            public InputStream getStream() {
                try {
                    return ResourceUtils
                        .getResourceAsStream(
                        resourcePath);
                } catch (IOException e) {
                    log.warn("Could not load resource:" +
                        resourcePath, e);
                    return IOUtils.toInputStream("");
                }
            }

        };
        return new StreamResource(streamSource, filename);
    }

}
