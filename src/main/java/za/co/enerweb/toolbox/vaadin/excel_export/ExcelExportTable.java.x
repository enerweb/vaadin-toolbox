package za.co.enerweb.toolbox.vaadin.excel_export;

import lombok.Data;
import lombok.EqualsAndHashCode;
import za.co.enerweb.toolbox.vaadin.ButtonBar;

import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

/**
 * Wraps a table for you to add a button bar with an export to excel button.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ExcelExportTable extends VerticalLayout {
    private static final long serialVersionUID = 1L;

    private final Table table;
    private final Button excelExportButton;
    private ButtonBar buttonBar = new ButtonBar();

    public ExcelExportTable(final Table table) {
        this(table, null);
    }

    public ExcelExportTable(final Table table, final String reportTitle) {
        this.table = table;

        excelExportButton = createExportButton(table, reportTitle);

        setSizeFull();
        addComponent(table);
        buttonBar.addComponent(excelExportButton);
        addComponent(buttonBar);
        setExpandRatio(table, 1);
    }

    public static Button createExportButton(final Table table) {
        return createExportButton(table, null);
    }

    public static Button createExportButton(final Table table,
        final String reportTitle) {
        // this is in the tableexport jar :-)
        final ThemeResource export = new ThemeResource(
            "../images/table-excel.png");
        Button excelExportButton = new Button("Export to Excel");
        excelExportButton.setIcon(export);

        excelExportButton.addClickListener(new ClickListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public void buttonClick(ClickEvent event) {

                ExcelExport excelExport = new ExcelExport(table);
                excelExport.excludeCollapsedColumns();
                if (reportTitle != null) {
                    excelExport.setReportTitle(reportTitle);
                }
                excelExport.export();
            }
        });
        return excelExportButton;
    }

}
