package za.co.enerweb.toolbox.vaadin.lazytable.filter;

public abstract class SqlFilterVisitor extends AFilterVisitor {
    protected StringBuilder sql = new StringBuilder();

    public void andStart(AndFilters f) {
        if (f.size() > 1) {
            sql.append("(");
        }
    }

    public void and(AndFilters f) {
        sql.append(" and ");
    }

    public void andEnd(AndFilters f) {
        if (f.size() > 1) {
            sql.append(")");
        }
    }

}
