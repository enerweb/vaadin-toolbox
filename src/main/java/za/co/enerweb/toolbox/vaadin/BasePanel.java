package za.co.enerweb.toolbox.vaadin;

import lombok.NoArgsConstructor;

import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;

/**
 * A good starting point for panels used to build up a screen.
 */
@NoArgsConstructor
public class BasePanel extends Panel {
    private static final long serialVersionUID = 1L;
    protected VerticalLayout mainLayout;

    {
        setStyleName(Runo.WINDOW_DIALOG);
        mainLayout = new VerticalLayout();
        mainLayout.setSpacing(false);
        mainLayout.setMargin(false);
        mainLayout.setImmediate(true);
        // don't set the layout size to full or you will not get scrollbars
        mainLayout.setSizeUndefined();

        setImmediate(true);
        setContent(mainLayout);
        // setScrollable(true);
    }

    public BasePanel(String caption) {
        super(caption);
    }

    public BasePanel(Component component) {
        mainLayout.addComponent(component);
    }

    public BasePanel(String caption, Component component) {
        super(caption);
        mainLayout.addComponent(component);
    }

    public void setMargin(boolean b) {
        if (b) {
            addStyleName(Styles.STYLE_PADDED_CONTENT);
        }
        mainLayout.setMargin(b);

    }

    public void setSpacing(boolean b) {
        mainLayout.setSpacing(b);
    }

    /**
     * call this if you want it full size and don't want scrollbars.
     * @return
     */
    public BasePanel fullSize() {
        setSizeFull();
        return setMainLayoutSizeFull();
    }

    public BasePanel setMainLayoutSizeFull() {
        mainLayout.setSizeFull();
        return this;
    }

    public BasePanel setMainLayoutStyleName(String style) {
        mainLayout.setStyleName(style);
        return this;
    }

    public VerticalLayout getMainLayout() {
        return mainLayout;
    }
}
