package za.co.enerweb.toolbox.vaadin;

import java.io.Serializable;

import com.vaadin.server.Resource;

/**
 * Generic interface for something that can create resources
 * like images.
 */
public interface IResourceProducer <T> extends Serializable{

    Resource getResource(T resourceKey);
}
