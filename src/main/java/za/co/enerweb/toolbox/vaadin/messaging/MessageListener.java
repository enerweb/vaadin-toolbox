package za.co.enerweb.toolbox.vaadin.messaging;

/*
 * TODO: rather make a @MessageListener annotation that can be put
 * on any method that takes something that extends Message,
 * then call that whenever the message can be cast to the declared type.
 * Will need to fix MRequestContextUpdate
 */
public interface MessageListener {

    void onMessage(Message message);
}
