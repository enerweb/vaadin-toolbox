package za.co.enerweb.toolbox.vaadin.highchart;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.IOUtils;

import com.invient.vaadin.charts.Color;
import com.invient.vaadin.charts.Color.RGB;

/**
 * This class is not thread safe.
 */
@Slf4j
public class ColorGenerator {

    private static Pattern colorPattern = Pattern.compile(
        "^(\\p{XDigit}{2})(\\p{XDigit}{2})(\\p{XDigit}{2})$");

    @Getter
    private static final List<RGB> defaultColors;

    // reed the default colors from a resource
    static {
        InputStream colorIs = Timeline.class.getClassLoader().getResourceAsStream(
            "za/co/enerweb/toolbox-colors.txt");
        List<RGB> defaultColors_ = null;
        if (colorIs != null) {
            try {
                List<String> colorStrings = IOUtils.readLines(colorIs);
                defaultColors_ = parseStrings(colorStrings);
            } catch (IOException e) {
                // could not read colors
            }
        }
        if (defaultColors_ == null) {
            defaultColors_ = Collections.emptyList();
        }
        defaultColors = defaultColors_; // can only do this once
    }

    protected static List<RGB> parseStrings(List<String> colorStrings) {
        List<RGB> defaultColors_ = new ArrayList<RGB>(colorStrings.size());
        for (String colorString : colorStrings) {
            if (colorString.length() == 6 ) {
                Matcher matcher = colorPattern.matcher(colorString);
                if (matcher.matches()) {
                    defaultColors_.add(
                    new Color.RGB(Integer.parseInt(matcher.group(1),16),
                        Integer.parseInt(matcher.group(2),16),
                        Integer.parseInt(matcher.group(3),16)));
                }
            }
        }
        return defaultColors_;
    }

    private Set<String> uniqueColors = new HashSet<String>();
    private List<RGB> colors;
    private Random random;

    /**
     * Use the available default colours, and generate more if required.
     * @param index
     * @return
     */
    public RGB getColor(int index) {
        if (colors == null) {
            colors = new ArrayList<RGB>(defaultColors);
            for (RGB rgb : colors) {
                uniqueColors.add(rgb.toString());
            }
        }
        // generate some more colors if required
        while (index >= colors.size()) {
            RGB rgb = getRandomColor();
            // try really hard to get unique colours
            for (int i = 0; i < 2000; i++) {
                if (uniqueColors.add(rgb.toString())) {
                    break;
                }
            }
            colors.add(rgb);
            //log.debug("added color: " + rgb);
        }
        return colors.get(index);
    }

    public RGB getRandomColor() {
        if (random == null) {
            random = new Random();
        }
        RGB rgb = new RGB(random.nextInt(100) + 100, random.nextInt(100) + 100,
            random.nextInt(100) + 100);
        return rgb;
    }
}
