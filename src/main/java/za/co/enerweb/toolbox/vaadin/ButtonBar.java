package za.co.enerweb.toolbox.vaadin;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.server.Resource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

/**
 * Enables consistent styling for button bars.
 * If you give it a ClickListener it will automatically set that on each button
 * when you call addButton.
 * To see which button was clicked compare it's caption eg.
 * event.getButton().getCaption()
 *
 * If you give it a IResourceProducer it will automatically set icons
 * using the key you provide.
 * It assumes that ALT+SHIFT is the key modifier (which is supported by
 * the most browsers).
 *
 * Tried to use a css layout to nicely float buttons, but could not
 * get the height to render correctly :(
 */
public class ButtonBar extends HorizontalLayout {

    private static final long serialVersionUID = 1L;
    private ClickListener clickListener;
    @SuppressWarnings("rawtypes")
    private IResourceProducer iconProducer;

    public ButtonBar() {
        setSizeUndefined();
        addStyleName("ebr-button-bar");
        addStyleName(Styles.STYLE_PADDED_CONTENT);
    }

    public ButtonBar(ClickListener clickListener) {
        this();
        this.clickListener = clickListener;
    }

    public ButtonBar(ClickListener clickListener,
        @SuppressWarnings("rawtypes") IResourceProducer iconProducer) {
        this();
        this.clickListener = clickListener;
        this.iconProducer = iconProducer;
    }

    @Override
    public void attach() {
        super.attach();
        boolean hasComponents = !this.components.isEmpty();
        setVisible(hasComponents);
        setMargin(true);
        setSpacing(true);
    }

    public void addLabel(String caption) {
        this.addComponent(new Label(caption));
    }

    @Override
    public void addComponent(Component c) {
        c.addStyleName("ebr-button-bar-content");
        super.addComponent(c);
    }

    public Button addButton(String caption) {
        return addButton(caption, "", null, null, null);
    }

    public Button addButton(String caption, String decription) {
        return addButton(caption, decription, null, null, null);
    }

    /**
     * @param caption
     * @param description eg. ""
     * @param keyCode eg. KeyCode.R see {@link KeyCode}
     * @param key eg. "R"
     * @param iconKey
     * @return
     */
    @SuppressWarnings("unchecked")
    public Button addButton(String caption, String description,
        Integer keyCode, String key, Object iconKey) {
        Resource resource = null;
        if (iconKey != null) {
            resource = iconProducer.getResource(iconKey);
        }
        return addButton(caption, description,
            keyCode, key, resource);
    }

    /**
     *
     * @param caption
     * @param description
     * @param shortcutKeyCode {@link KeyCode}
     * @param shortcutKey
     * @param icon
     * @return
     */
    public Button addButton(String caption, String description,
        Integer shortcutKeyCode, String shortcutKey, Resource icon) {
        Button button =
            createButton(caption, description, shortcutKeyCode, shortcutKey,
                icon);
        addComponent(button);
        return button;
    }

    public Button createButton(String caption, String description,
        Integer shortcutKeyCode, String shortcutKey, Resource icon) {
        Button button = new Button(caption);
        if (clickListener != null) {
            button.addListener(clickListener);
        }
        if (shortcutKeyCode != null) {
            button.setClickShortcut(shortcutKeyCode, ModifierKey.ALT,
                ModifierKey.SHIFT);
            if (!description.isEmpty()) {
                description += " ";
            }
        }
        if (shortcutKey != null) {
            // add it to the description because you can't tell vaadin to
            // underline a letter in a button capion yet:
            // http://dev.vaadin.com/ticket/1962
            // http://dev.vaadin.com/ticket/4521
            // http://dev.vaadin.com/ticket/875
            description += "(ALT+SHIFT+" + shortcutKey +")";
        }
        button.setDescription(description);
        if (icon != null) {
            button.setIcon(icon);
        }
        return button;
    }
}
