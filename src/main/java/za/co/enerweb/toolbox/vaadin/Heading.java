package za.co.enerweb.toolbox.vaadin;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;

public class Heading extends Label {

    private static final long serialVersionUID = 1L;
    private int level = 2;

    public Heading() {
        super();
    }

    public Heading(int level) {
        this.level = level;
    }

    /**
     * @param level 1, 2, 3 or 4 which corrisponds to h1, h2, h3 and h4 tags
     * @param content
     */
    public Heading(int level, String content) {
        /*
         * super("<h" + level + ">" + content + "</h" + level + ">",
         * Label.CONTENT_XHTML);
         */
        setContentMode(ContentMode.HTML);
        this.level = level;
        setContent(content);
    }

    /**
     * defaults to a h2 tag
     * @param content
     */
    public Heading(String content) {
        this(2, content);
    }

    public void setContent(String content) {
        setContentMode(ContentMode.HTML);
        // getPropertyDataSource().
        setValue("<h" + level + ">" +
            content + "</h" + level + ">");
    }

}
