package za.co.enerweb.toolbox.vaadin.lazytable;

import static lombok.AccessLevel.PRIVATE;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import za.co.enerweb.toolbox.string.StringUtils;
import za.co.enerweb.toolbox.vaadin.lazytable.SortSpec.SortProperty;

public class SortSpec extends ArrayList<SortProperty> {
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = PRIVATE)
    public class SortProperty {
        Object propertyId;
        boolean ascending = true;
    }

    public SortSpec() {
        super(1);
    }

    /**
     * @param sortPropertyIds
     * @param sortStates
     */
    public SortSpec(final Object[] sortPropertyIds,
        final boolean[] sortStates) {
        super(sortPropertyIds.length);
        if (sortPropertyIds.length != sortStates.length) {
            throw new IllegalArgumentException("Property names and sort " +
                "states should have equal length: "
                + sortPropertyIds.length + " != " + sortStates.length);
        }
        for (int i = 0; i < sortPropertyIds.length; i++) {
            addProperty(sortPropertyIds[i],
                sortStates[i]);
        }
    }

    public SortSpec addProperty(Object propertyId, boolean ascending) {
        add(new SortProperty(propertyId, ascending));
        return this;
    }

    /**
     * @param orderBy eg. "ORDER BY"
     * @param seperator eg. ", "
     * @param namePrefix eg. "t."
     * @param ascend eg. " ASC"
     * @param descend eg. " DESC"
     * @return empty string if there are no sort properties
     */
    public String toQueryString(String orderBy, String seperator,
        String namePrefix,
        String ascend, String descend) {
        List<String> orderByList = new ArrayList<String>(size());
        if (!isEmpty()) {
            for (SortProperty sp : this) {

                orderByList.add(namePrefix + sp.getPropertyId() +
                    (sp.isAscending() ? ascend : descend));
            }
            return orderBy + " " + StringUtils.join(orderByList, ", ");
        }
        return "";
    }
}
