package za.co.enerweb.toolbox.vaadin.highchart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import com.invient.vaadin.charts.Color.RGB;
import com.invient.vaadin.charts.Color.RGBA;
import com.invient.vaadin.charts.Gradient;
import com.invient.vaadin.charts.Gradient.LinearGradient.LinearColorStop;
import com.invient.vaadin.charts.InvientCharts;
import com.invient.vaadin.charts.InvientCharts.ChartClickEvent;
import com.invient.vaadin.charts.InvientCharts.ChartClickListener;
import com.invient.vaadin.charts.InvientCharts.ChartZoomEvent;
import com.invient.vaadin.charts.InvientCharts.ChartZoomListener;
import com.invient.vaadin.charts.InvientCharts.DateTimePoint;
import com.invient.vaadin.charts.InvientCharts.DateTimeSeries;
import com.invient.vaadin.charts.InvientCharts.DecimalPoint;
import com.invient.vaadin.charts.InvientCharts.Series;
import com.invient.vaadin.charts.InvientCharts.SeriesType;
import com.invient.vaadin.charts.InvientCharts.XYSeries;
import com.invient.vaadin.charts.InvientChartsConfig;
import com.invient.vaadin.charts.InvientChartsConfig.AreaConfig;
import com.invient.vaadin.charts.InvientChartsConfig.AxisBase.AxisTitle;
import com.invient.vaadin.charts.InvientChartsConfig.AxisBase.DateTimePlotBand;
import com.invient.vaadin.charts.InvientChartsConfig.AxisBase.DateTimePlotBand.DateTimeRange;
import com.invient.vaadin.charts.InvientChartsConfig.AxisBase.Grid;
import com.invient.vaadin.charts.InvientChartsConfig.AxisBase.NumberPlotLine;
import com.invient.vaadin.charts.InvientChartsConfig.AxisBase.NumberPlotLine.NumberValue;
import com.invient.vaadin.charts.InvientChartsConfig.CategoryAxis;
import com.invient.vaadin.charts.InvientChartsConfig.DateTimeAxis;
import com.invient.vaadin.charts.InvientChartsConfig.GeneralChartConfig.Margin;
import com.invient.vaadin.charts.InvientChartsConfig.GeneralChartConfig.ZoomType;
import com.invient.vaadin.charts.InvientChartsConfig.HorzAlign;
import com.invient.vaadin.charts.InvientChartsConfig.Legend;
import com.invient.vaadin.charts.InvientChartsConfig.Legend.Layout;
import com.invient.vaadin.charts.InvientChartsConfig.LineConfig;
import com.invient.vaadin.charts.InvientChartsConfig.MarkerState;
import com.invient.vaadin.charts.InvientChartsConfig.NumberYAxis;
import com.invient.vaadin.charts.InvientChartsConfig.Position;
import com.invient.vaadin.charts.InvientChartsConfig.SeriesConfig;
import com.invient.vaadin.charts.InvientChartsConfig.SeriesState;
import com.invient.vaadin.charts.InvientChartsConfig.SymbolMarker;
import com.invient.vaadin.charts.InvientChartsConfig.VertAlign;
import com.invient.vaadin.charts.InvientChartsConfig.XAxis;
import com.invient.vaadin.charts.InvientChartsConfig.YAxis;
import com.invient.vaadin.charts.InvientChartsConfig.YAxisDataLabel;
import com.invient.vaadin.charts.Paint;
import com.vaadin.data.Container;
import com.vaadin.ui.VerticalLayout;

@Getter
public class Timeline extends VerticalLayout implements ChartZoomListener,
    ChartClickListener {
    private static final String MASK_AFTER_BAND = "mask-after";

    private static final String MASK_BEFORE_BAND = "mask-before";

    private static final RGBA SEE_THROUGH_BLACK = new RGBA(0, 0, 0, 0.2f);

    private static final long serialVersionUID = 1L;

    private ColorGenerator colorGenerator = new ColorGenerator();

    @Data
    @AllArgsConstructor
    public static class YAxes {
        private NumberYAxis masterYAxis;
        private NumberYAxis detailYAxis;
    }

    public static enum PropertyId {
        // assumes it is a java.util.Date
        TIMESTAMP,
        // assumes it is a Number
        VALUE
    };

    private InvientCharts masterChart;
    private InvientCharts detailChart;

    private InvientChartsConfig masterConfig;
    private InvientChartsConfig detailConfig;

    private List<DateTimeAxis> masterXAxes = new ArrayList<DateTimeAxis>();
    private List<DateTimeAxis> detailXAxes = new ArrayList<DateTimeAxis>();

    private List<YAxis> masterYAxes = new ArrayList<YAxis>();
    private List<YAxis> detailYAxes = new ArrayList<YAxis>();

    private List<DateTimeSeries> serii = new ArrayList<DateTimeSeries>();

    private Date masterChartMinDate = null;
    private Date masterChartMaxDate = null;
    private Date detailChartMinDate = null;
    private Date detailChartMaxDate = null;

    @Setter
    private float detailChartRatio = 0.85f;

    public Timeline() {
        setSizeFull();
        setSpacing(true);
        initMasterChartConfig();
        initDetailChartConfig();
    }

    public void resetBounds() {
        detailChartMinDate = null;
        detailChartMaxDate = null;
        masterChartMinDate = null;
        masterChartMaxDate = null;
    }

    boolean firstTime = true;

    private DateTimeAxis detailXAxis;

    public void attach() {
        super.attach();
        removeAllComponents();

        // this seems to be a bug in the invient charts addon
        // because it sends the data as UTC, but never tells
        // highcharts about it.
        getApplication().getMainWindow().executeJavaScript(
            "Highcharts.setOptions({\n" +
            "    global: {\n" +
            "        useUTC: false\n" +
            "    }\n" +
            "});");

        // if (firstTime) {
        resetBounds(); // make sure we redraw with the correct bounds
        // if (masterChart == null) {
        createMasterChart();
        // }
        // if (detailChart == null) {
        createDetailChart();
        // }
        firstTime = false;
        // }
        // removeAllComponents();
        // addComponent(layout);
//        requestRepaint();


        // // // give the charts a chance to populate themselves
        // try {
        // Thread.sleep(100);
        // } catch (InterruptedException e) {
        // log.debug("interrupted :(", e);
        // }


//        updateDetailData();
        updateZoomPlotBand();

//        masterChart.setHeight("400px");

//        addComponent(detailChart);
//        addComponent(masterChart);


//        setExpandRatio(masterChart, 1 - detailChartRatio);
//        setExpandRatio(detailChart, detailChartRatio);

        VerticalLayout vl = new VerticalLayout();
        vl.setSizeFull();
        vl.setSpacing(true);
        vl.addComponent(detailChart);
        vl.addComponent(masterChart);
        vl.setExpandRatio(masterChart, 1 - detailChartRatio);
        vl.setExpandRatio(detailChart, detailChartRatio);
//        for (int i = 0; i < 2; i++) {
//            vl.addComponent(getSomeChart(i));
//        }
        addComponent(vl);

        masterChart.addListener((ChartZoomListener) this);
        masterChart.addListener((ChartClickListener) this);

//        detailChart.refresh();
//        masterChart.refresh();
//        requestRepaint();
    }

    private InvientCharts getSomeChart(int i) {
        InvientChartsConfig chartConfig = new InvientChartsConfig();
        chartConfig.getGeneralChartConfig().setType(SeriesType.LINE);
        chartConfig.getGeneralChartConfig().setMargin(new Margin());
        chartConfig.getGeneralChartConfig().getMargin().setRight(130);
        chartConfig.getGeneralChartConfig().getMargin().setBottom(25);

        chartConfig.getTitle().setX(-20);
        chartConfig.getTitle().setText("Monthly Average Temperature " + i);
        chartConfig.getSubtitle().setText("Source: WorldClimate.com");
        chartConfig.getTitle().setX(-20);

        CategoryAxis categoryAxis = new CategoryAxis();
        categoryAxis.setCategories(Arrays.asList("Jan", "Feb", "Mar", "Apr",
                "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"));
        LinkedHashSet<XAxis> xAxesSet = new LinkedHashSet<InvientChartsConfig.XAxis>();
        xAxesSet.add(categoryAxis);
        chartConfig.setXAxes(xAxesSet);

        NumberYAxis numberYAxis = new NumberYAxis();
        numberYAxis.setTitle(new AxisTitle("Temperature (°C)"));
        NumberPlotLine plotLine = new NumberPlotLine("TempAt0");
        plotLine.setValue(new NumberValue(0.0));
        plotLine.setWidth(1);
        plotLine.setZIndex(1);
        plotLine.setColor(new RGB(128, 128, 128));
        numberYAxis.addPlotLine(plotLine);
        LinkedHashSet<YAxis> yAxesSet = new LinkedHashSet<InvientChartsConfig.YAxis>();
        yAxesSet.add(numberYAxis);
        chartConfig.setYAxes(yAxesSet);

        Legend legend = new Legend();
        legend.setLayout(Layout.VERTICAL);
        Position legendPos = new Position();
        legendPos.setAlign(HorzAlign.RIGHT);
        legendPos.setVertAlign(VertAlign.TOP);
        legendPos.setX(-10);
        legendPos.setY(100);
        legend.setPosition(legendPos);
        legend.setBorderWidth(0);
        chartConfig.setLegend(legend);

        // Series data label formatter
        LineConfig lineCfg = new LineConfig();
        chartConfig.addSeriesConfig(lineCfg);
        // Tooltip formatter
        chartConfig
                .getTooltip()
                .setFormatterJsFunc(
                        "function() { "
                                + " return '<b>' + this.series.name + '</b><br/>' +  this.x + ': '+ this.y +'°C'"
                                + "}");

        InvientCharts chart = new InvientCharts(chartConfig);

        XYSeries seriesData = new XYSeries("Tokyo");
        seriesData.setSeriesPoints(getPoints(seriesData, 7.0, 6.9, 9.5, 14.5,
                18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6));
        chart.addSeries(seriesData);

        seriesData = new XYSeries("New York");
        seriesData.setSeriesPoints(getPoints(seriesData, -0.2, 0.8, 5.7, 11.3,
                17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5));
        chart.addSeries(seriesData);

        seriesData = new XYSeries("Berlin");
        seriesData.setSeriesPoints(getPoints(seriesData, -0.9, 0.6, 3.5, 8.4,
                13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0));
        chart.addSeries(seriesData);

        seriesData = new XYSeries("London");
        seriesData.setSeriesPoints(getPoints(seriesData, 3.9, 4.2, 5.7, 8.5,
                11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8));
        chart.addSeries(seriesData);

        chart.setSizeFull();
        chart.addStyleName("v-master");
        return chart;
    }
    private static LinkedHashSet<DecimalPoint> getPoints(Series series,
            double... values) {
        LinkedHashSet<DecimalPoint> points = new LinkedHashSet<DecimalPoint>();
        for (double value : values) {
            points.add(new DecimalPoint(series, value));
        }
        return points;
    }

    private void initDetailChartConfig() {
        detailConfig = new InvientChartsConfig();
        detailConfig.getGeneralChartConfig().setReflow(true);
        detailConfig.getGeneralChartConfig().setBorderWidth(0);
        detailConfig.getGeneralChartConfig().setMargin(new Margin());
        detailConfig.getGeneralChartConfig().getMargin().setBottom(120);
        detailConfig.getGeneralChartConfig().getMargin().setLeft(50);
        detailConfig.getGeneralChartConfig().getMargin().setRight(20);
        detailConfig.getGeneralChartConfig().setHeight(500);
//        detailConfig.getTitle().setText("Detail");
        detailConfig.getTitle().setText("");
        detailConfig.getTooltip().setShared(true);

        detailXAxis = new DateTimeAxis();
        // detailXAxis.setMin(getDetailChartMinDate());
        // detailXAxis.setMax(getDetailChartMaxDate());
        // detailXAxis.setMaxZoom(60 * 1000);
        detailXAxis.setTitle(new AxisTitle(""));
        detailXAxis.setShowLastLabel(true);
        detailXAxis.setShowFirstLabel(true);
        detailXAxes.add(detailXAxis);

        // Position legendPosition = new Position();
        // legendPosition.setVertAlign(VertAlign.TOP);
        // detailConfig.getLegend().setPosition(legendPosition);
        detailConfig.getCredit().setEnabled(false);

        // Plot options
        AreaConfig areaCfg = new AreaConfig();
        areaCfg.setShadow(false);

        SymbolMarker marker = new SymbolMarker(false);
        areaCfg.setMarker(marker);
        marker.setHoverState(new MarkerState());
        marker.getHoverState().setEnabled(true);
        marker.getHoverState().setRadius(10);

        detailConfig.addSeriesConfig(areaCfg);
    }

    private void initMasterChartConfig() {
        masterConfig = new InvientChartsConfig();

        masterConfig.getGeneralChartConfig().setReflow(true);
        masterConfig.getGeneralChartConfig().setBorderWidth(0);
        masterConfig.getGeneralChartConfig().setMargin(new Margin());
        masterConfig.getGeneralChartConfig().getMargin().setLeft(50);
        masterConfig.getGeneralChartConfig().getMargin().setRight(20);
        masterConfig.getGeneralChartConfig().setZoomType(ZoomType.X);
        masterConfig.getGeneralChartConfig().setClientZoom(false);
        masterConfig.getGeneralChartConfig().setHeight(80);
//        masterConfig.getTitle().setText("Master");
        masterConfig.getTitle().setText("");

        DateTimeAxis masterXAxis = new DateTimeAxis();
        // masterXAxis.setMaxZoom(3600000);

        masterXAxis.setTitle(new AxisTitle(""));
        masterXAxes.add(masterXAxis);
        addYAxis("", false);

        masterConfig.getTooltip().setFormatterJsFunc(
                "function() { return false; }");
        masterConfig.getLegend().setEnabled(false);
        masterConfig.getCredit().setEnabled(false);

        // Plot options
        AreaConfig areaCfg = new AreaConfig();
        List<LinearColorStop> colorStops =
            new ArrayList<Gradient.LinearGradient.LinearColorStop>();
        colorStops.add(new LinearColorStop(0, new RGB(69, 114, 167)));
        colorStops.add(new LinearColorStop(1, new RGBA(0, 0, 0, 0)));
        // Fill color
        areaCfg.setFillColor(new Gradient.LinearGradient(0, 0, 0, 70,
            colorStops));
        areaCfg.setLineWidth(1);
        areaCfg.setMarker(new SymbolMarker(false));
        areaCfg.setShadow(false);
        areaCfg.setEnableMouseTracking(false);
        areaCfg.setHoverState(new SeriesState());
        areaCfg.getHoverState().setLineWidth(1);
        masterConfig.addSeriesConfig(areaCfg);
    }

    private void updateDetailData() {
        LinkedHashSet<Series> detailSerii = new LinkedHashSet<Series>();
        for (DateTimeSeries masterSeries : serii) {

            AreaConfig areaCfg = deriveDetailSeriesConfig(masterSeries);

            DateTimeSeries detailSeries = new DateTimeSeries(
                masterSeries.getName(), SeriesType.AREA,
                areaCfg, masterSeries.isIncludeTime());

            // set to appropriate detail YAxis
            detailSeries.setYAxis(detailYAxes.get(
                masterYAxes.indexOf(masterSeries.getYAxis())));
            detailSeries.setXAxis(detailXAxis);

            LinkedHashSet<DateTimePoint> detailPoints =
                new LinkedHashSet<InvientCharts.DateTimePoint>();
            Date minDate = getDetailChartMinDate();
            Date maxDate = getDetailChartMaxDate();
            for (DateTimePoint point : masterSeries.getPoints()) {
                if (!minDate.after(point.getX()) &&
                    !point.getX().after(maxDate)) {
                    detailPoints.add(new DateTimePoint(detailSeries,
                        point.getX(),
                        point.getY()));
                }
            }
            detailSeries.setSeriesPoints(detailPoints);

            detailSerii.add(detailSeries);
        }
        detailChart.setSeries(detailSerii);
    }

    private AreaConfig deriveDetailSeriesConfig(DateTimeSeries masterSeries) {
        // get the colors from the master graph if possible
        AreaConfig areaCfg = new AreaConfig();
        SeriesConfig masterConfig = masterSeries.getConfig();
        if (masterConfig != null) {
            Paint color = masterConfig.getColor();
            areaCfg.setColor(color);
            if (color instanceof RGB) {
                RGB rgb = (RGB) color;

                areaCfg.setFillColor(new RGBA(rgb.getRed(),
                    rgb.getGreen(),
                    rgb.getBlue(), 0.3f));
            }
        }
        return areaCfg;
    }

    private void updateMasterChartRange() {
        masterChartMinDate = null;
        masterChartMaxDate = null;
        for (DateTimeSeries masterSeries : serii) {
            for (DateTimePoint dateTimePoint : masterSeries.getPoints()) {
                if (masterChartMinDate == null
                    || masterChartMinDate
                        .after(dateTimePoint.getX())) {
                    masterChartMinDate = dateTimePoint.getX();
                }

                if (masterChartMaxDate == null
                    || masterChartMaxDate
                        .before(dateTimePoint.getX())) {
                    masterChartMaxDate = dateTimePoint.getX();
                }
            }
        }
    }

    private InvientCharts createDetailChart() {
        detailChart = new InvientCharts(detailConfig);
//        detailChart.addStyleName("v-detail");

        // InvientChartsConfig does not clear it for you :(
        detailConfig.getXAxes().clear();
        detailConfig.getYAxes().clear();

        detailConfig.setXAxes(new LinkedHashSet<XAxis>(detailXAxes));
        detailConfig.setYAxes(new LinkedHashSet<YAxis>(detailYAxes));
        updateDetailData();
        detailChart.setSizeFull();
        return detailChart;
    }
    private InvientCharts createMasterChart() {
        masterChart = new InvientCharts(masterConfig);
//        masterChart.addStyleName("v-master");
        masterChart.addListener((ChartZoomListener) this);
//        masterChart.addListener((ChartClickListener) this);
        //masterChart.setDebugId("masterChart" + (idGenerator++));

        // InvientChartsConfig does not clear it for you :(
        masterConfig.getXAxes().clear();
        masterConfig.getYAxes().clear();

        masterConfig.setXAxes(new LinkedHashSet<XAxis>(masterXAxes));
        masterConfig.setYAxes(new LinkedHashSet<YAxis>(masterYAxes));

        updateZoomPlotBand();

        for (Series series : serii) {
            masterChart.addSeries(series);
        }
        masterChart.setSizeFull();

        return masterChart;
    }

    private void updateZoomPlotBand() {
        DateTimeAxis masterDateTimeAxis = (DateTimeAxis) masterChart
            .getConfig().getXAxes().iterator().next();
        masterDateTimeAxis.removePlotBand(MASK_BEFORE_BAND);
        DateTimePlotBand plotBandBefore = new DateTimePlotBand(
            MASK_BEFORE_BAND);
        plotBandBefore.setRange(new DateTimeRange(getMasterChartMinDate(),
            getDetailChartMinDate()));
        plotBandBefore.setColor(SEE_THROUGH_BLACK);
        masterDateTimeAxis.addPlotBand(plotBandBefore);

        masterDateTimeAxis.removePlotBand(MASK_AFTER_BAND);
        DateTimePlotBand plotBandAfter = new DateTimePlotBand(
            MASK_AFTER_BAND);
        plotBandAfter.setRange(new DateTimeRange(getDetailChartMaxDate(),
            getMasterChartMaxDate()));
        plotBandAfter.setColor(SEE_THROUGH_BLACK);
        masterDateTimeAxis.addPlotBand(plotBandAfter);
    }

    public void clearSeries() {
        serii.clear();
    }

    public void clearYAxis() {
        masterYAxes.clear();
        detailYAxes.clear();
    }

    public YAxis getDetailYAxis() {
        return getDetailYAxes().get(0);
    }

    @Override
    public void chartZoom(ChartZoomEvent chartZoomEvent) {
        double min = chartZoomEvent.getChartArea().getxAxisMin();
        double max = chartZoomEvent.getChartArea().getxAxisMax();

        detailChartMinDate = new Date((long) min);
        detailChartMaxDate = new Date((long) max);
        updateDetailData();
        updateZoomPlotBand();
        detailChart.refresh();
        masterChart.refresh();
    }

    @Override
    public void chartClick(ChartClickEvent chartClickEvent) {
        detailChartMinDate = masterChartMinDate;
        detailChartMaxDate = masterChartMaxDate;
        updateDetailData();
        updateZoomPlotBand();
        detailChart.refresh();
        masterChart.refresh();
    }

    /**
     * you should not need this from outside..
     */
    private boolean addSeries(DateTimeSeries e) {
        return serii.add(e);
    }

    /**
     * This adds it already, don't add it again!
     * I suppose we can add an api to take in a Vaadin DataSource
     * for adding datapoints...
     */
    public DateTimeSeries createSeries(String label, boolean includeTime) {
        // most of the config are done for the chart
        AreaConfig areaCfg = new AreaConfig();
        // we need to set the color otherwise the detailgraph can't clone it
        RGB color = colorGenerator.getColor(serii.size());
        areaCfg.setColor(color);
        DateTimeSeries dateTimeSeries = new DateTimeSeries(
            label, SeriesType.AREA, areaCfg, includeTime);
        addSeries(dateTimeSeries);
        return dateTimeSeries;
    }

    /**
     * Use this to easily port from other Timeline implentations.
     */
    public static void populateSeries(DateTimeSeries series,
        Container container) {
        for (Object itemId : container.getItemIds()) {
            Date time = (Date) container.getContainerProperty(itemId,
                PropertyId.TIMESTAMP).getValue();
            Number value = (Number) container.getContainerProperty(itemId,
                PropertyId.VALUE).getValue();
            // skip null values by default
            if (value != null) {
                series.addPoint(
                    new DateTimePoint(series, time, value.doubleValue()));
                // log.debug("{} {} {}", new Object[] {series.getName(), time,
                // value});
            }
        }
    }

    public YAxes addYAxis(String text, boolean opposite) {
        NumberYAxis masterYAxis = createYAxis("", opposite);
        masterYAxis.setLabel(new YAxisDataLabel(false));
        masterYAxes.add(masterYAxis);

        NumberYAxis detailYAxis = createYAxis(text, opposite);
        detailYAxis.setLabel(new YAxisDataLabel(true));
        detailYAxes.add(detailYAxis);
        return new YAxes(masterYAxis, detailYAxis);
    }

    private NumberYAxis createYAxis(String text, boolean opposite) {
        NumberYAxis yAxis = new NumberYAxis();
        yAxis.setGrid(new Grid());
        yAxis.getGrid().setLineWidth(0);
        yAxis.setTitle(new AxisTitle(text));
        yAxis.setOpposite(opposite);
        return yAxis;
    }

    // vvv lazy getters

    private Date getDetailChartMinDate() {
        if (detailChartMinDate == null) {
            // find the minimum date of all the series
            detailChartMinDate = getMasterChartMinDate();
        }
        return detailChartMinDate;
    }

    private Date getDetailChartMaxDate() {
        if (detailChartMaxDate == null) {
            // find the minimum date of all the series
            detailChartMaxDate = getMasterChartMaxDate();
        }
        return detailChartMaxDate;
    }

    private Date getMasterChartMinDate() {
        // find the minimum date of all the series
        if (masterChartMinDate == null) {
            updateMasterChartRange();
        }
        return masterChartMinDate;
    }

    private Date getMasterChartMaxDate() {
        // find the minimum date of all the series
        if (masterChartMaxDate == null) {
            updateMasterChartRange();
        }
        return masterChartMaxDate;
    }

}
