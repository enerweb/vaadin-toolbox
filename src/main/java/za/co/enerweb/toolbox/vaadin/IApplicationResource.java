package za.co.enerweb.toolbox.vaadin;

import com.vaadin.server.Resource;
import com.vaadin.ui.UI;

public interface IApplicationResource {
    public Resource getResource(UI ui);
}
