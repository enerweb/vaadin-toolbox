package za.co.enerweb.toolbox.vaadin;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.ui.ComboBox;

/**
 * Basically a ComboBox with completely different defaults, because I found
 * myself setting all this stuff on every ComboBox I make. Use a
 * Property.ValueChangeListener to get notified of changes
 */
public class DropDown extends ComboBox {

    private static final long serialVersionUID = 1L;

    public DropDown() {
        this(null, "");
    }

    public DropDown(String inputPrompt) {
        this(null, inputPrompt);
    }

    public DropDown(Property.ValueChangeListener listener) {
        this(listener, "");
    }

    public DropDown(Property.ValueChangeListener listener,
        String inputPrompt) {
        pageLength = 20;
        if (listener != null) {
            addListener(listener);
        }
        setInputPrompt(inputPrompt);
        setFilteringMode(Filtering.FILTERINGMODE_CONTAINS);
        setImmediate(true);
        setNullSelectionAllowed(false);
        // setMultiSelect(false);
        // setNewItemsAllowed(false);
        setWidth("100%");
    }

    public void setPageLength(int pageLength) {
        this.pageLength = pageLength;
    }

    public Item addItem(Object itemId, String caption) {
        Item ret = super.addItem(itemId);
        setItemCaption(itemId, caption);
        return ret;
    }
}
