package za.co.enerweb.toolbox.vaadin.messaging;

import static za.co.enerweb.toolbox.vaadin.messaging.MessageBusFactory.sendMessageDirectly;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.vaadin.ui.Component;

/**
 * if you get this message you can update the requestor's context by calling
 * updateContext with your current state eg. if you normally broadcast state
 * changes then re-send your state to this.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class MRequestContextUpdate extends Message {
    private MessageListener requestor;

    {
        super.setSynchronous(true);
    }

    public void updateContext(Component source, Message message) {
        sendMessageDirectly(source, message, requestor);
    }

    /**
     * @deprecated use MessageBusFactory.requestContextUpdate directly
     */
    @Deprecated
    public static void requestContextUpdate(Component source,
        MessageListener requestor) {
        MessageBusFactory.requestContextUpdate(source, requestor);
    }
}
