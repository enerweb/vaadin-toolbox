package za.co.enerweb.toolbox.vaadin.highchart;

public interface TimelineZoomListener {
    void timelineZoom(TimelineZoomEvent event);
}
