package za.co.enerweb.toolbox.vaadin;

import static lombok.AccessLevel.NONE;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;
import lombok.val;
import za.co.enerweb.toolbox.vaadin.messaging.MessageBusFactory;

import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.Resource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 * Common dialog features.
 * You have to focus on one of the child components to
 * make the actions work
 */
@Getter
@Setter
public abstract class Dialog extends Window implements Handler, ClickListener {

    private static final long serialVersionUID = 1L;

    @Setter(NONE)
    // Have the unmodified Enter key cause an event
    private Action actionEnter = new ShortcutAction("Enter",
        ShortcutAction.KeyCode.ENTER, null);

    @Setter(NONE)
    private Action actionEscape = new ShortcutAction("Enter",
        ShortcutAction.KeyCode.ESCAPE, null);

    private ButtonBar buttons = new ButtonBar(this);

    private VerticalLayout dialogLayout;

    private boolean buttonsAtTheBottom = true;

    private boolean dontSteelFocus = false;

    private boolean closeOnEnter = true;
    private boolean closeOnEscape = true;

    public Dialog() {
        dialogLayout = new VerticalLayout();
        this.setContent(dialogLayout);
    }

    public void attach() {
        super.attach();

        // Configure the windows layout; by default a VerticalLayout
        dialogLayout = (VerticalLayout) getContent();
        dialogLayout.addStyleName("ebr-dialog");

        // set size if needed
        if (getWidth() >= 0 || getHeight() >= 0) {
            dialogLayout.setSizeFull();
        } else {
            dialogLayout.setSizeUndefined();
        }

        dialogLayout.setMargin(true);
        buttons.setSpacing(true);
        buttons.setMargin(false);
        buttons.setSizeUndefined();
        buttons.removeAllComponents(); // guard against multiple attaches

        Component content = getDialogContent();
        if (buttonsAtTheBottom) {
            dialogLayout.addComponent(content);
        }
        dialogLayout.addComponent(buttons);
        dialogLayout.setComponentAlignment(buttons, Alignment.BOTTOM_RIGHT);
        if (!buttonsAtTheBottom) {
            dialogLayout.addComponent(content);
        }
        dialogLayout.setExpandRatio(content, 1f);
        dialogLayout.setExpandRatio(buttons, 0f);

        center();
        addActionHandler(this);
    }

    protected void addComponent(Component component) {
        dialogLayout.addComponent(component);
    }

    protected void addButton(final Button button) {
        buttons.addComponent(button);
        buttons.setComponentAlignment(button, Alignment.BOTTOM_RIGHT);
    }

    protected Button createButton(String caption, String description,
        Integer shortcutKeyCode, String shortcutKey, Resource icon) {
        return buttons.createButton(
            caption, description, shortcutKeyCode, shortcutKey, icon);
    }

    /*
     * (non-Javadoc)
     * @see com.vaadin.event.Action.Handler#getActions(java.lang.Object,
     * java.lang.Object)
     */
    @Override
    public Action[] getActions(final Object target, final Object sender) {
        val ret = new ArrayList<Action>(2);
        if (closeOnEnter) {
            ret.add(actionEnter);
        }
        if (closeOnEscape) {
            ret.add(actionEscape);
        }
        return ret.toArray(new Action[ret.size()]);
    }

    /*
     * (non-Javadoc)
     * Override this to do other actions
     * @see
     * com.vaadin.event.Action.Handler#handleAction(com.vaadin.event.Action,
     * java.lang.Object, java.lang.Object)
     */
    @Override
    public void handleAction(final Action action, final Object sender,
        final Object target) {
        if ((action == actionEnter && closeOnEnter) ||
            (action == actionEscape && closeOnEscape)) {
            close();
        }
    }

    /**
     * the component will be added to the window
     * @return
     */
    protected abstract Component getDialogContent();

    /*
     * Let subclasses decide if they wanna implement this
     */
    @Override
    public void buttonClick(ClickEvent event) {
    }

    /**
     * Actually show the dialog and register it with the application's
     * messagebus.
     */
    public void show() {
        show(getUI());
    }

    public void show(UI ui) {
        ui.addWindow(this);
        MessageBusFactory.registerComponent(ui, this);
    }

    /**
     * Actually show the dialog.
     * @param someAttachedComponent just used to get the application
     */
    public void show(Component someAttachedComponent) {
        show(someAttachedComponent.getUI());
    }

    public void close() {
        super.close();
    }
}
