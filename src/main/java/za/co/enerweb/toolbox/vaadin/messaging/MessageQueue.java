package za.co.enerweb.toolbox.vaadin.messaging;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MessageQueue extends Thread {
    @Data
    @AllArgsConstructor
    private static class QueueMessage {
        MessageListener listener;
        Message message;
    };

    private static BlockingQueue<QueueMessage> messageQueue =
        new ArrayBlockingQueue<QueueMessage>(20, true);

    private MessageQueue() {
        super("ScreenMessageQueue");
        setDaemon(true);
        setPriority(Thread.currentThread().getPriority() - 1);
        start();
    }

    public static void queueMessage(MessageListener listener,
        Message message) {
        messageQueue.add(new QueueMessage(listener, message));
    }

    @Override
    public void run() {
        try {
            while(true) {
                QueueMessage qmsg = messageQueue.take();
                try {
                    qmsg.listener.onMessage(qmsg.message);
                } catch (Exception e) {
                    log.error("An error occured while sending "
                        + qmsg.message + " to " + qmsg.listener, e);
                }
            }
        } catch (InterruptedException intEx) {
            log.debug(getName() + " was interrupted! " +
                "Last one out, turn out the lights!");
        }
    }

}
