package za.co.enerweb.toolbox.vaadin;

import com.vaadin.ui.Label;

public class HorizontalLine extends Label {

    private static final long serialVersionUID = 1L;

    /**
     * defaults to a hr tag
     * @param content
     */
    public HorizontalLine() {
        this(1);
    }

    /**
     * @param level 1, 2, 3 or 4 which corrisponds to h1, h2, h3 and h4 tags
     * @param content
     */
    public HorizontalLine(int lineWidth) {
        super("<hr style='height:" + lineWidth + "px' />",
            Label.CONTENT_XHTML);
    }

}
