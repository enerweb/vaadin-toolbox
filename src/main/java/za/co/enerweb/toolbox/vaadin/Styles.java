package za.co.enerweb.toolbox.vaadin;

/**
 * A set of styles that is assumed to be in the theme
 */
public interface Styles {
    /**
     * Used to give layouts a small padding
     */
    public static final String STYLE_PADDED_CONTENT = "padded-content";
}
