package za.co.enerweb.toolbox.vaadin.lazytable;

import lombok.AllArgsConstructor;
import za.co.enerweb.toolbox.reflection.PropertyUtils;

import com.vaadin.data.Property;

@AllArgsConstructor
public class PrettyProperty<T> implements Property {

    private transient ALazyDataProducer<T> aLazyDataProducer;
    private transient PropertyInfo propertyInfo;
    private T dataObject;
    private String displayProperty;
    private boolean readonly;

    public Object getValue() {
        return aLazyDataProducer.getPropertyValue(dataObject,
            propertyInfo.id());
    }

    public void setValue(Object newValue) throws ReadOnlyException {
        aLazyDataProducer.setPropertyValue(dataObject,
            propertyInfo.id(), newValue);
    }

    public String toString() {
        Object value = getValue();
        if (value == null) {
            return null;
        }
        if (displayProperty != null) {
            return PropertyUtils.getProperty(value, displayProperty)
                .toString();
        }
        return value.toString();
    }

    public Class<?> getType() {
        return propertyInfo.type();
    }

    public boolean isReadOnly() {
        return readonly;
    }

    public void setReadOnly(boolean readonly) {
        this.readonly = readonly;
    }

}
