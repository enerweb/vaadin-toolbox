package za.co.enerweb.toolbox.vaadin.highchart;

import static lombok.AccessLevel.PROTECTED;
import static org.apache.commons.math.util.MathUtils.round;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import com.invient.vaadin.charts.Color.RGB;
import com.invient.vaadin.charts.Color.RGBA;
import com.invient.vaadin.charts.Gradient;
import com.invient.vaadin.charts.Gradient.LinearGradient.LinearColorStop;
import com.invient.vaadin.charts.InvientCharts;
import com.invient.vaadin.charts.InvientCharts.ChartClickEvent;
import com.invient.vaadin.charts.InvientCharts.ChartClickListener;
import com.invient.vaadin.charts.InvientCharts.ChartZoomEvent;
import com.invient.vaadin.charts.InvientCharts.ChartZoomListener;
import com.invient.vaadin.charts.InvientCharts.DateTimePoint;
import com.invient.vaadin.charts.InvientCharts.DateTimeSeries;
import com.invient.vaadin.charts.InvientCharts.Series;
import com.invient.vaadin.charts.InvientChartsConfig;
import com.invient.vaadin.charts.InvientChartsConfig.AreaConfig;
import com.invient.vaadin.charts.InvientChartsConfig.AxisBase.AxisTitle;
import com.invient.vaadin.charts.InvientChartsConfig.AxisBase.DateTimePlotBand;
import com.invient.vaadin.charts.InvientChartsConfig.AxisBase.DateTimePlotBand.DateTimeRange;
import com.invient.vaadin.charts.InvientChartsConfig.AxisBase.Grid;
import com.invient.vaadin.charts.InvientChartsConfig.DateTimeAxis;
import com.invient.vaadin.charts.InvientChartsConfig.GeneralChartConfig.ZoomType;
import com.invient.vaadin.charts.InvientChartsConfig.MarkerState;
import com.invient.vaadin.charts.InvientChartsConfig.NumberYAxis;
import com.invient.vaadin.charts.InvientChartsConfig.SeriesConfig;
import com.invient.vaadin.charts.InvientChartsConfig.SeriesState;
import com.invient.vaadin.charts.InvientChartsConfig.SymbolMarker;
import com.invient.vaadin.charts.InvientChartsConfig.XAxis;
import com.invient.vaadin.charts.InvientChartsConfig.YAxis;
import com.invient.vaadin.charts.InvientChartsConfig.YAxisDataLabel;
import com.invient.vaadin.charts.Paint;
import com.vaadin.data.Container;
import com.vaadin.ui.VerticalLayout;

@Slf4j
@Getter
public class Timeline extends VerticalLayout implements ChartZoomListener,
    ChartClickListener {
    private static final String MASK_AFTER_BAND = "mask-after";
    private static final String MASK_BEFORE_BAND = "mask-before";
    private static final String LINE_AFTER_BAND = "line-after";
    private static final String LINE_BEFORE_BAND = "line-before";

    private static final RGBA BLACK = new RGBA(0, 0, 0, 1f);
    private static final RGBA SEE_THROUGH_BLACK = new RGBA(0, 0, 0, 0.2f);

    private static final long serialVersionUID = 1L;

    @Setter
    private TimelineType TimelineType;

    private ColorGenerator colorGenerator = new ColorGenerator();
    @Setter
    private int roundDecimals = 3;

    @Data
    @AllArgsConstructor
    public static class YAxes {
        private NumberYAxis masterYAxis;
        private NumberYAxis detailYAxis;
    }

    public static enum PropertyId {
        // assumes it is a java.util.Date
        TIMESTAMP,
        // assumes it is a Number
        VALUE
    };

    private InvientCharts masterChart;
    private InvientCharts detailChart;

    @Setter(PROTECTED)
    private InvientChartsConfig masterConfig;
    @Setter(PROTECTED)
    private InvientChartsConfig detailConfig;

    private List<XAxis> masterXAxes = new ArrayList<XAxis>();
    private List<XAxis> detailXAxes = new ArrayList<XAxis>();

    private List<YAxis> masterYAxes = new ArrayList<YAxis>();
    private List<YAxis> detailYAxes = new ArrayList<YAxis>();

    private List<DateTimeSeries> serii = new ArrayList<DateTimeSeries>();

    private Date masterChartMinDate = null;
    private Date masterChartMaxDate = null;
    private Date detailChartMinDate = null;
    private Date detailChartMaxDate = null;

    private transient Map<TimelineZoomListener, Object> timelineZoomListeners;

    @Setter
    private float detailChartRatio = 0.85f;

    private boolean addMasterGraph;

    public Timeline() {
        timelineZoomListeners =
            new WeakHashMap<TimelineZoomListener, Object>(1);
        setSizeFull();
        TimelineType = TimelineType.TIMELINE_AREA;
    }
    public Timeline(boolean addMasterGraph){
       this();
       this.addMasterGraph =  addMasterGraph;
    }

    /**
     * overide this if you need to do stuff before the configs get init
     */
    protected void initConfigs() {
        if (masterConfig == null) {
            initMasterChartConfig();
        }
        if (detailConfig == null) {
            initDetailChartConfig();
        }
    }

    public void resetBounds() {
        detailChartMinDate = null;
        detailChartMaxDate = null;
        masterChartMinDate = null;
        masterChartMaxDate = null;
    }

    boolean firstTime = true;

    public void attach() {
        super.attach();
        init();
    }

    public void init() {
        // only do this now to give subclasses a chance to init themselves
        initConfigs();

        // this seems to be a bug in the invient charts addon
        // because it sends the data as UTC, but never tells
        // highcharts about it.
        getUI().getPage().getJavaScript().execute(
            "Highcharts.setOptions({\n" +
            "    global: {\n" +
            "        useUTC: false\n" +
            "    }\n" +
            "});");

        // if (firstTime) {
        resetBounds(); // make sure we redraw with the correct bounds
        // if (masterChart == null) {
        createMasterChart();
        masterChart.addListener((ChartZoomListener) this);
        masterChart.addListener((ChartClickListener) this);
        // }
        // if (detailChart == null) {
        createDetailChart();
        // }
        removeAllComponents();


        if (addMasterGraph){
            addComponent(masterChart);

        }
        addComponent(detailChart);
        if (addMasterGraph){
           setExpandRatio(masterChart, 1 - detailChartRatio);
           setExpandRatio(detailChart, detailChartRatio);
        }
        firstTime = false;
        // }
        // removeAllComponents();
        // addComponent(layout);
        requestRepaint();
        // requestRepaintAll();
        // masterChart.setSizeFull();
        // detailChart.setSizeFull();

        updateDetailData();
        updateZoomPlotBand();
        detailChart.refresh();
        masterChart.refresh();

        // // // give the charts a chance to populate themselves
        // try {
        // Thread.sleep(100);
        // } catch (InterruptedException e) {
        // log.debug("interrupted :(", e);
        // }
    }

    protected void initDetailChartConfig() {
        detailConfig = new InvientChartsConfig();
        detailConfig.getGeneralChartConfig().setReflow(true);
        detailConfig.getTitle().setText("");
        detailConfig.getTooltip().setShared(true);

        DateTimeAxis detailXAxis = new DateTimeAxis();
        // detailXAxis.setMin(getDetailChartMinDate());
        // detailXAxis.setMax(getDetailChartMaxDate());
        // detailXAxis.setMaxZoom(60 * 1000);
        detailXAxis.setTitle(new AxisTitle(""));
        detailXAxes.add(detailXAxis);

        // Position legendPosition = new Position();
        // legendPosition.setVertAlign(VertAlign.TOP);
        // detailConfig.getLegend().setPosition(legendPosition);
        detailConfig.getCredit().setEnabled(false);

        // Plot options
        AreaConfig areaCfg = new AreaConfig();
        areaCfg.setShadow(false);

        SymbolMarker marker = new SymbolMarker(false);
        areaCfg.setMarker(marker);
        marker.setHoverState(new MarkerState());
        marker.getHoverState().setEnabled(true);
        marker.getHoverState().setRadius(10);

        detailConfig.addSeriesConfig(areaCfg);
    }

    protected void initMasterChartConfig() {
        masterConfig = new InvientChartsConfig();

        masterConfig.getGeneralChartConfig().setReflow(true);
        // masterConfig.getGeneralChartConfig().setHeight(70);
        masterConfig.getGeneralChartConfig().setZoomType(ZoomType.X);
        masterConfig.getGeneralChartConfig().setClientZoom(false);
        masterConfig.getTitle().setText("");

        DateTimeAxis masterXAxis = new DateTimeAxis();
        masterXAxis.setShowLastLabel(true);
//        masterXAxis.setMaxZoom(3600000);
        masterXAxis.setMaxZoom(360000000);

        masterXAxis.setTitle(new AxisTitle(""));
        masterXAxes.add(masterXAxis);
        addYAxis("", false);

        masterConfig.getLegend().setEnabled(false);
        masterConfig.getCredit().setEnabled(false);

        // Plot options
        AreaConfig areaCfg = new AreaConfig();
        List<LinearColorStop> colorStops =
            new ArrayList<Gradient.LinearGradient.LinearColorStop>();
        colorStops.add(new LinearColorStop(0, new RGB(69, 114, 167)));
        colorStops.add(new LinearColorStop(1, new RGBA(0, 0, 0, 0)));
        // Fill color
        areaCfg.setFillColor(new Gradient.LinearGradient(0, 0, 0, 70,
            colorStops));
        areaCfg.setLineWidth(1);
        areaCfg.setMarker(new SymbolMarker(false));
        areaCfg.setShadow(false);
        areaCfg.setEnableMouseTracking(true);//TOGGLED
        areaCfg.setHoverState(new SeriesState());
        areaCfg.getHoverState().setLineWidth(1);
        masterConfig.addSeriesConfig(areaCfg);
    }

    private void updateDetailData() {
        if (masterConfig == null || detailChart == null) {
            // not initialized yet!
            return;
        }
        LinkedHashSet<Series> detailSerii = new LinkedHashSet<Series>();
        for (DateTimeSeries masterSeries : serii) {

            detailSerii.add(deriveDetailSeries(masterSeries));
        }
        detailChart.setSeries(detailSerii);
    }

    public Series deriveDetailSeries(DateTimeSeries masterSeries) {
        AreaConfig areaCfg = deriveDetailSeriesConfig(masterSeries);

        DateTimeSeries detailSeries = new DateTimeSeries(
            masterSeries.getName(), TimelineType.getType(),
            areaCfg, masterSeries.isIncludeTime());

        // set to appropriate detail YAxis
        int masterYaxesIndex = masterYAxes.indexOf(masterSeries.getYAxis());
        if (masterYaxesIndex != -1) {
            detailSeries.setYAxis(detailYAxes.get(
                masterYaxesIndex));
        }

        Date minDate = getDetailChartMinDate();
        Date maxDate = getDetailChartMaxDate();

        LinkedHashSet<DateTimePoint> detailPoints =
            new LinkedHashSet<InvientCharts.DateTimePoint>();
        for (DateTimePoint point : masterSeries.getPoints()) {
            Date date = point.getX();
            if (!minDate.after(date) &&
                !date.after(maxDate)) {
                detailPoints.add(new DateTimePoint(detailSeries,
                    date,
                    round(point.getY(), roundDecimals)));
            }
        }

        detailSeries
            .setSeriesPoints(detailPoints);
        return detailSeries;
    }

    protected AreaConfig deriveDetailSeriesConfig(DateTimeSeries masterSeries) {
        // get the colors from the master graph if possible
        AreaConfig areaCfg = new AreaConfig();
        SeriesConfig masterConfig = masterSeries.getConfig();
        if (masterConfig != null) {
            Paint color = masterConfig.getColor();
            areaCfg.setColor(color);
            if (color instanceof RGB) {
                RGB rgb = (RGB) color;

                areaCfg.setFillColor(new RGBA(rgb.getRed(),
                    rgb.getGreen(),
                    rgb.getBlue(), 0.3f));
            }
        }
        return areaCfg;
    }

    private void updateMasterChartRange() {
        masterChartMinDate = null;
        masterChartMaxDate = null;
        for (DateTimeSeries masterSeries : serii) {
            for (DateTimePoint dateTimePoint : masterSeries.getPoints()) {
                if (masterChartMinDate == null
                    || masterChartMinDate
                        .after(dateTimePoint.getX())) {
                    masterChartMinDate = dateTimePoint.getX();
                }

                if (masterChartMaxDate == null
                    || masterChartMaxDate
                        .before(dateTimePoint.getX())) {
                    masterChartMaxDate = dateTimePoint.getX();
                }
            }
        }
    }

    private InvientCharts createDetailChart() {

        // InvientChartsConfig does not clear it for you :(
        detailConfig.getXAxes().clear();
        detailConfig.getYAxes().clear();

        detailConfig.setXAxes(new LinkedHashSet<XAxis>(detailXAxes));
        detailConfig.setYAxes(new LinkedHashSet<YAxis>(detailYAxes));
        detailChart = new InvientCharts(detailConfig);
        updateDetailData();
        detailChart.setSizeFull();
        return detailChart;
    }

    private InvientCharts createMasterChart() {
        masterChart = new InvientCharts(masterConfig);
        // masterChart.setDebugId("masterChart" + (idGenerator++));

        // InvientChartsConfig does not clear it for you :(
        masterConfig.getXAxes().clear();
        masterConfig.getYAxes().clear();

        masterConfig.setXAxes(new LinkedHashSet<XAxis>(masterXAxes));
        masterConfig.setYAxes(new LinkedHashSet<YAxis>(masterYAxes));

        updateZoomPlotBand();

        for (Series series : serii) {
            masterChart.addSeries(series);
        }
        masterChart.setSizeFull();
        return masterChart;
    }

    private void updateZoomPlotBand() {
        DateTimeAxis masterDateTimeAxis = (DateTimeAxis) masterChart
            .getConfig().getXAxes().iterator().next();
        Date detailChartMinDate = getDetailChartMinDate();
        Date masterChartMinDate = getMasterChartMinDate();
        Date detailChartMaxDate = getDetailChartMaxDate();
        Date masterChartMaxDate = getMasterChartMaxDate();

        if (masterChartMaxDate == null || masterChartMinDate == null) {
            return;
        }

        long delta = (long) ((masterChartMaxDate.getTime()
            - masterChartMinDate.getTime()) * 0.003);
        Date lineBefore = new Date(detailChartMinDate.getTime() - delta);
        Date lineAfter = new Date(detailChartMaxDate.getTime() + delta);

        updatePlotBandMask(masterDateTimeAxis, MASK_BEFORE_BAND,
            masterChartMinDate, lineBefore, SEE_THROUGH_BLACK);

        updatePlotBandMask(masterDateTimeAxis, MASK_AFTER_BAND,
            lineAfter, masterChartMaxDate, SEE_THROUGH_BLACK);

        // add some lines to highlight where we are.
        updatePlotBandMask(masterDateTimeAxis, LINE_BEFORE_BAND,
            lineBefore, new Date(detailChartMinDate.getTime() + delta),
            BLACK);

        updatePlotBandMask(masterDateTimeAxis, LINE_AFTER_BAND,
            new Date(detailChartMaxDate.getTime() - delta), lineAfter,
            BLACK);
    }

    public void updatePlotBandMask(DateTimeAxis masterDateTimeAxis,
        String bandId, Date min, Date max, Paint paint) {
        masterDateTimeAxis.removePlotBand(bandId);
        DateTimePlotBand plotBand = new DateTimePlotBand(bandId);
        plotBand.setRange(new DateTimeRange(min, max));
        plotBand.setColor(paint);
        masterDateTimeAxis.addPlotBand(plotBand);
    }

    public void clearSeries() {
        serii.clear();
    }

    public void clearYAxis() {
        masterYAxes.clear();
        detailYAxes.clear();
    }

    public YAxis getDetailYAxis() {
        return getDetailYAxes().get(0);
    }

    @Override
    public void chartZoom(ChartZoomEvent chartZoomEvent) {
        double min = chartZoomEvent.getChartArea().getxAxisMin();
        double max = chartZoomEvent.getChartArea().getxAxisMax();

        setZoomRange(new Date((long) min), new Date((long) max));
    }

    @Override
    public void chartClick(ChartClickEvent chartClickEvent) {
        setZoomRange(masterChartMinDate, masterChartMaxDate);
    }

    public void setZoomRange(Date minDate, Date maxDate) {
        detailChartMinDate = minDate;
        detailChartMaxDate = maxDate;
        updateDetailData();
        updateZoomPlotBand();
        detailChart.refresh();
        masterChart.refresh();
        for (TimelineZoomListener czl : timelineZoomListeners.keySet()) {
            czl.timelineZoom(new TimelineZoomEvent(this, minDate, maxDate));
        }
    }

    /**
     * you should not need this from outside..
     */
    private boolean addSeries(DateTimeSeries e) {
        return serii.add(e);
    }

    /**
     * This dynamically adds a point to a series
     * This is very handy for graphs to update on the fly
     *
     * A refresher is needed on the client side as well, see
     * https://vaadin.com/directory#addon/refresher
     * @param label, the same label as createSeries
     * @param dateTime date
     * @param value
     */
    public void addPointToSeries(String label,Date dateTime,double value) {
        addPointToSeries( label, dateTime, value,true);
    }

    public void addPointToSeries(String label,Date dateTime,double value,boolean shift) {
        InvientCharts.DateTimeSeries seriesData =
            (InvientCharts.DateTimeSeries) masterChart
                .getSeries(label);
        if (seriesData == null){
            log.warn("Could not find series "+label+" in time line");
            return;
        }
        seriesData.addPoint(new InvientCharts.DateTimePoint(
            seriesData,
            dateTime, value), true);
    }
    public void addPointToSeries(DateTimeSeries seriesData, Date dateTime,
        double value) {
        seriesData.addPoint(new InvientCharts.DateTimePoint(
            seriesData,
            dateTime, value), true);
    }

    /**
     * This adds it already, don't add it again!
     * I suppose we can add an api to take in a Vaadin DataSource
     * for adding datapoints...
     */
    public DateTimeSeries createSeries(String label, boolean includeTime) {
        // most of the config are done for the chart
        AreaConfig areaCfg = new AreaConfig();
        // we need to set the color otherwise the detailgraph can't clone it
        RGB color = colorGenerator.getColor(serii.size());
        areaCfg.setColor(color);
        DateTimeSeries dateTimeSeries = new DateTimeSeries(
            label, TimelineType.getType(), areaCfg, includeTime);
        addSeries(dateTimeSeries);
        return dateTimeSeries;
    }

    public DateTimeSeries createSeries(String label, boolean includeTime,
        YAxes yAxes) {
        DateTimeSeries ret = createSeries(label, includeTime);
        ret.setYAxis(yAxes.getMasterYAxis());
        return ret;
    }

    /**
     * Use this to easily port from other Timeline implentations.
     */
    public static void populateSeries(DateTimeSeries series,
        Container container) {
        for (Object itemId : container.getItemIds()) {
            Date time = (Date) container.getContainerProperty(itemId,
                PropertyId.TIMESTAMP).getValue();
            Number value = (Number) container.getContainerProperty(itemId,
                PropertyId.VALUE).getValue();
            // skip null values by default
            if (value != null) {
                series.addPoint(
                    new DateTimePoint(series, time, value.doubleValue()));
//                log.debug("{} {} {}", new Object[] {series.getName(), time,
//                    value});
            }
        }
    }

    public YAxes addYAxis(String text, boolean opposite) {
        NumberYAxis masterYAxis = createYAxis("", opposite);
        masterYAxis.setLabel(new YAxisDataLabel(false));
        masterYAxes.add(masterYAxis);

        NumberYAxis detailYAxis = createYAxis(text, opposite);
        detailYAxis.setLabel(new YAxisDataLabel(true));
        detailYAxes.add(detailYAxis);
        return new YAxes(masterYAxis, detailYAxis);
    }

    private NumberYAxis createYAxis(String text, boolean opposite) {
        NumberYAxis yAxis = new NumberYAxis();
        yAxis.setGrid(new Grid());
        yAxis.getGrid().setLineWidth(0);
        yAxis.setTitle(new AxisTitle(text));
        yAxis.setOpposite(opposite);
        return yAxis;
    }

    // vvv lazy getters

    protected Date getDetailChartMinDate() {
        if (detailChartMinDate == null) {
            // find the minimum date of all the series
            detailChartMinDate = getMasterChartMinDate();
        }
        return detailChartMinDate;
    }

    protected Date getDetailChartMaxDate() {
        if (detailChartMaxDate == null) {
            // find the minimum date of all the series
            detailChartMaxDate = getMasterChartMaxDate();
        }
        return detailChartMaxDate;
    }

    private Date getMasterChartMinDate() {
        // find the minimum date of all the series
        if (masterChartMinDate == null) {
            updateMasterChartRange();
        }
        return masterChartMinDate;
    }

    private Date getMasterChartMaxDate() {
        // find the minimum date of all the series
        if (masterChartMaxDate == null) {
            updateMasterChartRange();
        }
        return masterChartMaxDate;
    }

    public void addTimelineZoomListener(TimelineZoomListener czl) {
        timelineZoomListeners.put(czl, null);
    }




}

