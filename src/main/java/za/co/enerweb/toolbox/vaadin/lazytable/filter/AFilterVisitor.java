package za.co.enerweb.toolbox.vaadin.lazytable.filter;

public abstract class AFilterVisitor {

    public void visit(PropertyFilter f) {

    }

    public void andStart(AndFilters f) {

    }

    public void andEnd(AndFilters f) {

    }

    public void and(AndFilters andFilters) {
    }
}
