package za.co.enerweb.toolbox.vaadin.lazytable;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import lombok.val;
import lombok.extern.slf4j.Slf4j;

import org.joda.time.DateTime;

import za.co.enerweb.toolbox.datetime.DateFormats;
import za.co.enerweb.toolbox.reflection.PropertyUtils;
import za.co.enerweb.toolbox.string.StringUtils;

@Slf4j
public abstract class ABeanProducer<T> extends ALazyDataProducer<T> {
    private Collection<PropertyDescriptor> propertieDescriptors;

    private List<?> propertyInfosTemplate;

    public ABeanProducer(Class<T> dataObjectClass) {
        super(dataObjectClass);
    }

    /**
     * @param dataClass
     * @param propertyInfosTemplate may contain Strings which will imply
     *        propertyIds or PropertyInfo objects (they may be partial too)
     *        If specified, only properties in this list will be used in the
     *        order
     *        given.
     */
    public ABeanProducer(Class<T> dataClass, List<?> propertyInfosTemplate) {
        super(dataClass);
        this.propertyInfosTemplate = propertyInfosTemplate;
    }

    private Collection<PropertyDescriptor> getPropertieDescriptors() {
        if (propertieDescriptors == null) {
            propertieDescriptors =
                PropertyUtils.getAllReadablePropertyDescriptors(
                    getDataObjectClass());
        }
        return propertieDescriptors;
    }

    public List<PropertyInfo> getBasicPropertyInfos() {
        Collection<PropertyDescriptor> pds = getPropertieDescriptors();
        val ret = new ArrayList<PropertyInfo>(pds.size());
        for (PropertyDescriptor pd : pds) {
            String name = pd.getName();
            ret.add(new PropertyInfo(name, StringUtils
                .variableName2Caption(name), pd.getPropertyType()));
        }
        return ret;
    }

    private void populatePropertyInfo(List<PropertyInfo> basicPropertyInfos,
        PropertyInfo pi) {
        // if (pi.isGenerated()) {
        // return;
        // }
        for (PropertyInfo propertyInfo : basicPropertyInfos) {
            if (propertyInfo.id().equals(pi.id())) {
                if (pi.caption() == null) {
                    pi.caption(propertyInfo.caption());
                }
                if (pi.type() == null) {
                    pi.type(propertyInfo.type());
                }
                return;
            }
        }
        // throw new IllegalStateException(
        // "Could not find property with id=" + pi.getId()
        // + ", one of the following was expected:\n"
        // + StringUtils.join(basicPropertyInfos));
    }

    public List<PropertyInfo> getPropertyInfos() {
        List<PropertyInfo> basicPropertyInfos = getBasicPropertyInfos();
        if (propertyInfosTemplate == null) {
            return basicPropertyInfos;
        }
        val ret = new ArrayList<PropertyInfo>(propertyInfosTemplate.size());
        for (Object pit : propertyInfosTemplate) {
            PropertyInfo pi;
            if (pit instanceof String) {
                pi = new PropertyInfo((String) pit);
            } else if (pit instanceof PropertyInfo) {
                pi = (PropertyInfo) pit;
            } else {
                throw new IllegalStateException(
                    "Invalid object type for propertyInfosTemplate:" +
                    "'" + pit.getClass().getName()
                    + "' only String and PropertyInfo is supported.");
            }
            populatePropertyInfo(basicPropertyInfos, pi);
            ret.add(pi);
        }
        return ret;
    }

    /**
     * Override this if you want to generate values
     */
    public Object getPropertyValue(T dataObject,
        Serializable propertyId) {
        try {
            Object value = PropertyUtils.getProperty(
                dataObject, (String) propertyId);
            if (value instanceof Date) {
                value = DateFormats.DATE_TIME.print(new DateTime((Date) value));
            }
            return value;
        } catch (Exception e) {
            // ignore properties we can't get the value of
            // System.out.println("exception: " + e.getClass());
        }
        return null;
    }

    public void setPropertyValue(T dataObject,
        Serializable propertyId, Object value) {
        PropertyUtils.setProperty(dataObject, (String) propertyId, value);
    }

    public T newInstance() {
        try {
            return getDataObjectClass().newInstance();
        } catch (Exception e) {
            throw new UnsupportedOperationException(
                "Could not instantiate bean: "
                    + getDataObjectClass().getName(), e);
        }
    }
}
