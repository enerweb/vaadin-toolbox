package za.co.enerweb.toolbox.vaadin;

import java.io.Serializable;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;

import za.co.enerweb.toolbox.throwable.ThrowableUtils;

import com.vaadin.server.Page;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

/**
 * Consistently show exception notifications
 */
// TODO: change to a fluent api eg. new NotificationUtil(this)
// .message().description().exception().warning()
// (also have .clear() if people reuse them)
@Slf4j
public class NotificationUtil implements Serializable {

    private static final long serialVersionUID = 1L;
    private final Component component;

    public NotificationUtil(final Component component) {
        this.component = component;
    }

    public NotificationUtil(// final UI ui
    ) {
        final UI ui = UI.getCurrent();
        if (ui == null) {
            // this should only happen while testing
            log.warn("Application is null");
            component = new Window();
        } else {
            component = ui.getContent();
        }
    }

    private Page getAnchorComponent() {
        if (component == null) {
            log.warn("NotificationUtils is being used with unattached " +
                "components.");
            return Page.getCurrent();
        }
        UI ui = component.getUI();
        if (ui == null) {
            log.warn("NotificationUtils is being used with unattached " +
                "components.");
            return Page.getCurrent();
        }
        return ui.getPage();
    }

    public void note(String msg) {
        msg = newlinesToBr(msg);
        log.info(msg);
        Notification.show(msg, Type.HUMANIZED_MESSAGE);
    }

    public void btw(String msg) {
        msg = newlinesToBr(msg);
        log.info(msg);
        Notification.show(msg, Type.TRAY_NOTIFICATION);
    }

    public void btw(String msg, int delayMsec) {
        msg = newlinesToBr(msg);
        log.info(msg);
        Notification n = new Notification(msg,
            Type.TRAY_NOTIFICATION);
        n.setDelayMsec(delayMsec);
        n.show(getAnchorComponent());
    }

    public void error(String msg) {
        msg = newlinesToBr(msg);
        log.error(msg);
        Notification.show(msg, Type.ERROR_MESSAGE);
    }

    public void error(String msg, final Exception e) {
        msg = newlinesToBr(msg);
        log.error(msg, e);
        Notification.show(msg, getDetailFromException(e), Type.ERROR_MESSAGE);
    }

    public void warning(final String msg) {
        warning(msg, null);
    }

    public void warning(String msg, final Exception e) {
        msg = newlinesToBr(msg);
        if (e != null) {
            log.warn(msg, e);
            Notification.show(msg, getDetailFromException(e),
                Type.WARNING_MESSAGE);
        } else {
            log.warn(msg);
            Notification.show(msg, Type.WARNING_MESSAGE);
        }
    }

    public String newlinesToBr(String txt) {
        return txt.replaceAll("[\\n\\r]+", "<BR/>");
    }

    public String getDetailFromException(final Exception e) {
        return StringUtils.join(ThrowableUtils.getMessages(e),
            "<BR/>");
    }
}
