package za.co.enerweb.toolbox.vaadin.lazytable;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Setter;

import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;
import org.vaadin.addons.lazyquerycontainer.QueryFactory;

import za.co.enerweb.toolbox.vaadin.NotificationUtil;
import za.co.enerweb.toolbox.vaadin.lazytable.filter.AFilter;

import com.vaadin.data.Item;

public class LazyQueryFactory implements QueryFactory, Serializable {

    private QueryDefinition definition;
    @Setter
    private ALazyDataProducer aLazyDataProducer;
    @Setter
    private List<PropertyInfo> propertyInfos;
    @Setter
    private Class<?> entityClass;

    @Setter
    private Map<Object, String> colDisplayProperties;
    private final LazyTable<?> lazyTable;

    // private BeanTable beanTable;

    public LazyQueryFactory(LazyTable<?> lazyTable) {
        this.lazyTable = lazyTable;
    }

    public LazyQueryFactory(LazyTable<?> lazyTable,
        ALazyDataProducer iDataProducer) {
        this(lazyTable);
        this.aLazyDataProducer = iDataProducer;
    }

    @Override
    public Query constructQuery(QueryDefinition definition) {
        this.definition = definition;

        // public Query constructQuery(final Object[] sortPropertyIds,
        // final boolean[] sortStates) {
        if (aLazyDataProducer == null) {
            throw new IllegalStateException("You need to set a iDataProducer " +
                "before using this factory.");
        }

        // XXX: should obtain the current filter from the table
        AFilter aFilter = aLazyDataProducer.getFilter();

        try {
            SortSpec sortSpec;
            Object[] sortPropertyIds = definition.getSortPropertyIds();
            if (sortPropertyIds.length > 0) {
                sortSpec = new SortSpec(sortPropertyIds,
                    definition.getSortPropertyAscendingStates());
            } else {
                sortSpec = aLazyDataProducer.getDefaultSortSpec();
            }

            return new LazyQuery(aLazyDataProducer, propertyInfos,
                entityClass, colDisplayProperties,
                definition, sortSpec,
                aFilter, lazyTable);
        } catch (Exception e) {
            new NotificationUtil(lazyTable).error("Could not load table.", e);
            return new Query() {

                @Override
                public int size() {
                    return 0;
                }

                @Override
                public void saveItems(List<Item> arg0, List<Item> arg1,
                    List<Item> arg2) {
                }

                @Override
                public List<Item> loadItems(int arg0, int arg1) {
                    return null;
                }

                @Override
                public boolean deleteAllItems() {
                    return false;
                }

                @Override
                public Item constructItem() {
                    return null;
                }
            };
        }
    }


    // public void setBeanTable(BeanTable beanTable) {
    // this.beanTable = beanTable;
    // }

}
