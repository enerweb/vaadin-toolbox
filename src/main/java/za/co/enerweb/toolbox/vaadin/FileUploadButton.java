package za.co.enerweb.toolbox.vaadin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;

import za.co.enerweb.toolbox.vaadin.internal.FileUploadDialog;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;

/**
 * Simplified vaadin upload button
 */
@Slf4j
public class FileUploadButton extends HorizontalLayout implements Receiver {
    @Getter
    private Upload upload;
    private FileUploadDialog dialog;
    private File file;
    private final FileReceiver fileReceiver;
    private final String caption;

    protected FileUploadDialog createFileUploadDialog() {
        return new FileUploadDialog(this);
    }

    public FileUploadButton(FileReceiver fileReceiver) {
        this(null, fileReceiver);
    }

    public FileUploadButton(String caption, FileReceiver fileReceiver) {
        super();
        this.setMargin(false);
        this.setSpacing(false);
        addStyleName("vt-upload-button");
        this.caption = caption;
        this.fileReceiver = fileReceiver;
        setupUpload();
    }

    protected void setupUpload() {
        removeAllComponents();
        upload = new Upload();
        addComponent(upload);
        dialog = createFileUploadDialog();
        // Make uploading start immediately when file is selected
        upload.setImmediate(true);

        dialog.setFileReceiver(fileReceiver);
        upload.setButtonCaption(caption);
        upload.setReceiver(this);
    }

    /**
     * Override this only if you really want to stream the data directly
     * to your application
     */
    @Override
    public final OutputStream receiveUpload(final String filename,
        final String MIMEType) {
        try {
            file = createTempFile(filename);
            return new FileOutputStream(file);
        } catch (IOException e) {
            log.warn("Could not receive file " + filename, e);
        }
        return null;
    }

    protected File createTempFile(final String filename) throws IOException {
        return File.createTempFile(filename, "tmp");
    }

    public File getFile() {
        if (file == null) {
            log.debug("If you override receiveUpload, then a temp file will "
                + "not be created.");
            throw new RuntimeException("No file has been uploaded.");
        }
        return file;
    }

    public String getFileAsText() throws IOException {
        return FileUtils.readFileToString(getFile(), "UTF8");
    }

    public byte[] getFileAsBytes() throws IOException {
        return FileUtils.readFileToByteArray(getFile());
    }

    public InputStream getFileAsInputStream() throws IOException {
        return FileUtils.openInputStream(getFile());
    }

    public interface FileReceiver {
        void receiveFile(FileUploadButton sourceButton);
    }
}
