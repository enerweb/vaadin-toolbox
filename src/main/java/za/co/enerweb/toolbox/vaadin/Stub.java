package za.co.enerweb.toolbox.vaadin;

import com.vaadin.ui.Label;

public class Stub extends Label {

    private static final long serialVersionUID = 1L;

    public Stub() {
        this("Watch this space!");
    }

    public Stub(String caption) {
        super(formatCaption(caption));
        setSizeFull();
        // make it a little more visible
        addStyleName("ebr-env-var-heading-cell");
        addStyleName("ebr-env-var-grid");
        this.setContentMode(CONTENT_RAW);
    }

    private static String formatCaption(String caption) {
        return "<h1>" + caption + "</h1>";
    }

    public void setCaption(Object value) {
        super.setValue(formatCaption(value.toString()));
    }
}
