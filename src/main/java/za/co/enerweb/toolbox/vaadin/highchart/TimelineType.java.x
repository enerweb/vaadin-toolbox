package za.co.enerweb.toolbox.vaadin.highchart;

import lombok.Getter;

import com.invient.vaadin.charts.InvientCharts.SeriesType;

public enum TimelineType {

    TIMELINE_LINE(SeriesType.LINE),
    TIMELINE_SPLINE(SeriesType.SPLINE),
    TIMELINE_SCATTER(SeriesType.SCATTER),
    TIMELINE_AREA(SeriesType.AREA),
    TIMELINE_AREASPLINE(SeriesType.AREASPLINE),
    TIMELINE_BAR(SeriesType.BAR),
    TIMELINE_COLUMN(SeriesType.COLUMN),
    TIMELINE_PIE(SeriesType.PIE);

    TimelineType(SeriesType type){
        this.type = type;
    }
    @Getter
    private SeriesType type;
}