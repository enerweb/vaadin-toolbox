package za.co.enerweb.toolbox.vaadin.lazytable;

import static za.co.enerweb.toolbox.reflection.PropertyUtils.getAllProperties;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

import org.vaadin.addons.lazyquerycontainer.LazyQueryContainer;
import org.vaadin.addons.lazyquerycontainer.LazyQueryView;
import org.vaadin.addons.lazyquerycontainer.QueryItemStatus;
import org.vaadin.addons.lazyquerycontainer.QueryItemStatusColumnGenerator;

import za.co.enerweb.toolbox.string.StringUtils;
import za.co.enerweb.toolbox.vaadin.ButtonBar;
import za.co.enerweb.toolbox.vaadin.ConfirmationDialog;
import za.co.enerweb.toolbox.vaadin.lazytable.internal.LazyTableTable;

import com.vaadin.data.Item;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.shared.ui.MultiSelectMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

/**
 * Obtains it's data from an ALazyDataProducer.
 * T is the data object class type (represents a row).
 */
@Slf4j
@SuppressWarnings("unchecked")
public class LazyTable<T> extends VerticalLayout
    implements ClickListener {

    // button captions
    private String RELOAD = "Reload";
    private String EDIT = "Edit";
    private String REMOVE = "Remove";
    private String ADD = "Add";
    @Getter
    private String itemName;
    private String _itemName;
    private String _itemNames;

    private ButtonBar buttons = new ButtonBar(this);

    private ArrayList<VisibleColumn> visibleColumns =
        new ArrayList<VisibleColumn>();

    @Data
    @AllArgsConstructor
    private class VisibleColumn {
        Object id;
        String caption;
        Class<?> type;
    }

    private LazyQueryContainer container;
    private transient LazyQueryFactory lazyQueryFactory;

    private Class<T> dataObjectClass;
    private Table table;

    /**
     * Setting page length 0 disables paging. The page length defaults to 20.
     */
    @Setter
    private int pageLength = 20;
    @Setter
    private int cacheRate = 10;

    private Map<Object, String> colDisplayProperties =
        new HashMap<Object, String>();

    @Setter
    private boolean editable = false;
    @Setter
    private boolean addAndRemovable = false;
    @Getter
    private transient ALazyDataProducer<T> aLazyDataProducer;
    private List<PropertyInfo> propertyInfos;
    private Label countLabel = new Label();

    @Setter
    private transient FormFieldFactory formFieldFactory = null;

    public LazyTable(String itemName) {
        this(null, itemName);
    }

    public LazyTable(
        ALazyDataProducer<T> aLazyDataProducer, Object idPropertyId) {
        this(aLazyDataProducer, "item", idPropertyId);
    }

    public LazyTable(
        ALazyDataProducer<T> aLazyDataProducer, String itemName,
        Object idPropertyId) {
        this.idPropertyId = idPropertyId;
        setLazyDataProducer(aLazyDataProducer);
        this.itemName = itemName;
        this._itemName = " " + itemName;
        _itemNames = _itemName + "(s)";
        EDIT = EDIT + _itemName;
        REMOVE = REMOVE + _itemName;
        ADD = ADD + _itemName;
        setSizeFull();
        lazyQueryFactory = new LazyQueryFactory(this);
    }

    public void setItemNamePlural(String pItemNames) {
        _itemNames = " " + pItemNames;
    }

    public void setLazyDataProducer(ALazyDataProducer<T> aLazyDataProducer) {
        if (aLazyDataProducer != null) {
            this.aLazyDataProducer = aLazyDataProducer;
            this.dataObjectClass = aLazyDataProducer.getDataObjectClass();
            propertyInfos = aLazyDataProducer.getPropertyInfos();
            // populateTable();
            if (getUI() != null) {
                _populate();
            }
        }
    }

    private List<ItemClickListener> itemClickListeners =
        new ArrayList<ItemClickListener>(1);
    private Object idPropertyId;

    public void addListener(ItemClickListener itemClickListener) {
        itemClickListeners.add(itemClickListener);
    }

    private void updateVisibleColumns() {
        visibleColumns = new ArrayList<VisibleColumn>(propertyInfos.size());
        colDisplayProperties.clear();
        for (PropertyInfo propertyInfo : propertyInfos) {
            Object propertyId = propertyInfo.id();
            Class<?> type = propertyInfo.type();
            if (type == null) {
                log.debug("Could not find type for property with Id: "
                    + propertyId);
                propertyInfo.type(String.class);
            }
            // don't add linked entities
            Field field;
            try {
                if (type == null || !Iterable.class.isAssignableFrom(type)
                    && !Map.class.isAssignableFrom(type)) {
                    String caption = propertyInfo.caption();
                    if (caption == null) {
                        // generate caption if one is not provided
                        caption = StringUtils
                            .variableName2Caption("" + propertyId);
                    }
                    visibleColumns.add(new VisibleColumn(propertyId, caption,
                        propertyInfo.type()));
                    if (type != null) {
                    Collection<String> colProperties = getAllProperties(type);
                    checkDisplayProperties(propertyId, colProperties,
                        "caption", "name", "key", "val", "id");
                    }
                }
            } catch (Exception e) {
                log.debug("Could not find type of field " + propertyId, e);
            }
        }
    }

    public void checkDisplayProperties(Object propertyId,
        Collection<String> colProperties,
        String... possibleDisplayProperties) {
        for (String pdp : possibleDisplayProperties) {
            if (colProperties.contains(pdp)) {
                colDisplayProperties.put(propertyId, pdp);
                break;
            }
        }
    }

    @Override
    public void attach() {
        super.attach();
        populate();
    }

    /**
     * Draw the table and everything from scratch
     */
    public void reset() {
        buttons.removeAllComponents();
        removeAllComponents();
        container = null;
        table = null;
        populate();
    }

    public void populate() {
        _populate();
    }

    /*
     * Avoid recursion when we call _poplate,
     * we don't want to call child's populate again..
     */
    private void _populate() {
        if (this.getComponentIndex(buttons) < 0) {
            buttons.removeAllComponents();
            buttons.addButton(RELOAD);
            if (formFieldFactory == null) {
                formFieldFactory = new SimpleFieldFactory(editable);
            }
            if (editable) {
                buttons.addButton(EDIT);
            }
            if (addAndRemovable || editable) {
                buttons.addButton(ADD, "", KeyCode.A, "A", null);
                buttons.addButton(REMOVE, "", KeyCode.R, "R", null);
            }
            buttons.addComponent(countLabel);
            addComponent(buttons);
            addButtons(buttons);
        }
        if (table == null) {
            populateTable();
        }
    }

    protected void addButtons(ButtonBar buttons) {
    }

    /**
     * Load the data from scratch
     * refresh() is final :(
     */
    public void reload() {
        aLazyDataProducer.reload();
        if (container != null) {
            container.refresh();
        }
        updateCountLabel();
    }

    public synchronized void populateTable() {
        if (dataObjectClass == null || aLazyDataProducer == null ||
            getUI() == null) { // not attached
            container = null; // make sure this gets called when attached
            return;
        }
        if (table != null) {
            removeComponent(table);
        }
        table = new LazyTableTable(aLazyDataProducer);
        for (val l : itemClickListeners) {
            table.addItemClickListener(l);
        }
        addComponent(table);
        setExpandRatio(table, 1f);
        table.setSizeFull();

        table.setPageLength(pageLength);
        table.setCacheRate(cacheRate); // number of pages to cache
        // above and below
        table.setSelectable(true);
        table.setMultiSelect(true);
        table.setMultiSelectMode(MultiSelectMode.DEFAULT);
        table.setColumnCollapsingAllowed(true);
        table.setBuffered(false);

        if (isModifyable()) {
            table.setImmediate(true);
        }

        lazyQueryFactory.setALazyDataProducer(aLazyDataProducer);
        lazyQueryFactory.setEntityClass(dataObjectClass);
        lazyQueryFactory.setColDisplayProperties(colDisplayProperties);
        lazyQueryFactory.setPropertyInfos(propertyInfos);

        // lazyQueryFactory.setBeanTable(this);

        updateVisibleColumns();

        container =
            new LazyQueryContainer(lazyQueryFactory, idPropertyId, 20, true);
        container.addContainerProperty(
            LazyQueryView.PROPERTY_ID_ITEM_STATUS,
            QueryItemStatus.class, QueryItemStatus.None, true, false);
        for (VisibleColumn vc : visibleColumns) {
            container.addContainerProperty(vc.id, vc.type, "",
                !editable, true);
        }
        String spacer = "$_spacer_$";
        container.addContainerProperty(spacer, String.class, ""); // spacer
        table.setContainerDataSource(container);

        visibleColumns.add(0, new VisibleColumn(
            LazyQueryView.PROPERTY_ID_ITEM_STATUS, "", String.class));
        visibleColumns.add(new VisibleColumn(
            spacer, "", String.class));

        Object[] visibleColumnIds = new Object[visibleColumns.size()];
        String[] visibleColumnLabels = new String[visibleColumns.size()];
        int i = 0;
        for (VisibleColumn vc : visibleColumns) {
            visibleColumnIds[i] = vc.id;
            visibleColumnLabels[i] = vc.caption;
            i++;
        }
        table.setVisibleColumns(visibleColumnIds);
        table.setColumnHeaders(visibleColumnLabels);

        for (VisibleColumn vc : visibleColumns) {
            // table.setColumnWidth(protertyId, 250);
            table.setColumnExpandRatio(vc.id, 0.3f);
        }
        table.setColumnExpandRatio(spacer, 0.7f);
        table.setColumnWidth(LazyQueryView.PROPERTY_ID_ITEM_STATUS, 16);

        table.addGeneratedColumn(LazyQueryView.PROPERTY_ID_ITEM_STATUS,
                new QueryItemStatusColumnGenerator());
        table.setEditable(editable);

        updateCountLabel();
    }

    public boolean isModifyable() {
        return editable || addAndRemovable;
    }

    private void updateCountLabel() {
        countLabel.setValue(container.size() + _itemNames);
    }

    public Set<Integer> getSelectedIndexes() {
        Set<Integer> selectedIndexes = (Set<Integer>) table.getValue();
        return selectedIndexes;
    }

    public Set<T> getSelected() {
        final Set<Integer> sel = getSelectedIndexes();
        val ret = new HashSet<T>(sel.size());
        for (Integer indexslashid : sel) {
            ret.add((T) getItem(indexslashid));
        }
        return ret;
    }

    public T getItem(Integer indexslashid) {
        return (T) LazyQuery.fromItem((Item) container.getItem(indexslashid));
    }

    @Override
    public void buttonClick(ClickEvent event) {
        String caption = event.getButton().getCaption();
        if (caption.equals(RELOAD)) {
            reload();
        } else if (ADD.equals(caption)) {
            Object newItemId = container.addItem();
            T newItem = (T) getItem((Integer) newItemId);
            container.commit();
            reload();
            onItemAdded(newItem);
            // this isn't implemented properly yet and should be opt in
            // Object newItem = table.getItem(newItemId);
            // ObjectEditorDialog dialog =
            // new ObjectEditorDialog(itemName, new SimpleFieldFactory(
            // editable), // formFieldFactory,
            // LazyQuery.beanItemfromItem((CompositeItem) newItem));
            // dialog.show(this);
        } else if (REMOVE.equals(caption)) {
            final Set<Integer> sel = getSelectedIndexes();
            ConfirmationDialog d = new ConfirmationDialog(
                "Remove" + _itemNames, "Are you sure that you want to " +
                    "delete the " + sel.size() + " selected "
                    + _itemNames);
            d.setWidth("250px");
            d.addConfirmationListener(new ConfirmationDialog.OkListener() {

                @Override
                public void onOk() {
                    for (Integer t : sel) {
                        container.removeItem(t);
                        container.commit();
                        reload();
                    }
                }
            });
            d.show(getUI());
        }

    }

    /**
     * Override this if you want to do something when a new item has been
     * added eg. open an editor dialog..
     * @param newItemId
     */
    protected void onItemAdded(T newItem) {
    }

}
