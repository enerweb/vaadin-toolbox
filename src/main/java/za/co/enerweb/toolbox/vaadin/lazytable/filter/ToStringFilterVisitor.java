package za.co.enerweb.toolbox.vaadin.lazytable.filter;

import java.util.Date;

import org.joda.time.DateTime;

import za.co.enerweb.toolbox.datetime.DateFormats;


public class ToStringFilterVisitor extends SqlFilterVisitor {

    public ToStringFilterVisitor() {
    }

    public void visit(PropertyFilter fp) {
        Object value = fp.getValue();
        if (value instanceof Date) {
            value = DateFormats.DATE_TIME.print(new DateTime((Date) value));
        }
        sql.append(fp.getPropertyCaption() + " "
            + fp.getOperator().getSql() + " " + value);
    }

    public String toString() {
        return sql.toString();
    }
}
