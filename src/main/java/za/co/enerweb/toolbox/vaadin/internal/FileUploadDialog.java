package za.co.enerweb.toolbox.vaadin.internal;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;

import za.co.enerweb.toolbox.vaadin.Dialog;
import za.co.enerweb.toolbox.vaadin.FileUploadButton;
import za.co.enerweb.toolbox.vaadin.FileUploadButton.FileReceiver;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.VerticalLayout;

/**
 * Dialog that shows file upload progress
 */
@Slf4j
public class FileUploadDialog extends Dialog {

    private static final long serialVersionUID = 1L;

    private VerticalLayout layout;

    private FileUploadButton uploadButton;
    private Upload upload;


    private String fileName = "uploading_file";
    private ProgressIndicator progressIndicator = new ProgressIndicator();
    private Label status = new Label("");
    @Setter
    private FileReceiver fileReceiver;

    public FileUploadDialog(final FileUploadButton uploadButton) {
        this.uploadButton = uploadButton;
        this.upload = uploadButton.getUpload();
        setWidth("50%");
        setCaption("File upload dialog.");
        setModal(true);

        // Configure the window's layout; by default a VerticalLayout
        layout = new VerticalLayout();
        layout.setStyleName("ebr-confirmation-dialog");

        layout.setMargin(true);
        layout.setSpacing(true);

        progressIndicator.setWidth("100%");

        this.setClosable(false);

        final Button cancelProcessing = new Button("Cancel");
        cancelProcessing.addClickListener(new Button.ClickListener() {
            private static final long serialVersionUID = 1L;

            public void buttonClick(final ClickEvent event) {
                cancelUpload();
            }
        });
        cancelProcessing.setStyleName("small");

        addComponent(status);
        addComponent(progressIndicator);
        addComponent(cancelProcessing);

        upload.addStartedListener(new Upload.StartedListener() {
            public void uploadStarted(final StartedEvent event) {
                // This method gets called immediately after upload is started
                uploadButton.setVisible(false);
                FileUploadDialog.this.show(uploadButton);
                progressIndicator.setValue(0f);
                progressIndicator.setPollingInterval(500);
                fileName = event.getFilename();
                status.setValue("Uploading file '" + fileName + "'");
            }
        });

        upload.addSucceededListener(new Upload.SucceededListener() {
            public void uploadSucceeded(final SucceededEvent event) {
                status.setValue("");
                if (FileUploadDialog.this.fileReceiver != null) {
                    FileUploadDialog.this.fileReceiver.receiveFile(
                        uploadButton);
                }
            }
        });

        upload.addFailedListener(new Upload.FailedListener() {
            public void uploadFailed(final FailedEvent event) {
                status.setValue("");
                showMessage("Uploading interrupted");
            }
        });

        upload.addFinishedListener(new Upload.FinishedListener() {
            public void uploadFinished(final FinishedEvent event) {
                // This method gets called always when the upload finished,
                // either succeeding or failing
                uploadButton.setVisible(true);
                FileUploadDialog.this.close();
            }
        });
    }


    private void cancelUpload() {
        upload.interruptUpload();
        FileUtils.deleteQuietly(uploadButton.getFile());
    }

    private void showMessage(final String msg) {
        Notification.show(msg);
    }

    @Override
    public Component getDialogContent() {
        return layout;
    }
}
