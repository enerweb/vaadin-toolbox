package za.co.enerweb.toolbox.vaadin.lazytable.filter;


/*
 * FIXME probably needs lots of work here, it should be more like a where
 * clause. but try not to get too tricky.
 * may need to replace the list of acceptable values for a propertyId
 * Composite Pattern to represent it, Visitor Pattern to process it
 * AFilter
 * NullFilter (does nothing)
 * AndFilter
 * OrFilter
 * PropertyFilter(propertyId[, EQUALS|LIKE|GREATER_OR_EQUALS|...],value)
 */
public abstract class AFilter {

    public void acceptVisitor(AFilterVisitor visitor) {

    }
}
