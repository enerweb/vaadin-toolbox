package za.co.enerweb.toolbox.vaadin;

import za.co.enerweb.toolbox.datetime.DateFormats;

import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.PopupDateField;

public class DateTimeField extends PopupDateField {
    public static final String DATE_FORMAT = DateFormats.DATE_TIME_FORMAT;

    public DateTimeField() {
        setResolution(InlineDateField.RESOLUTION_SEC);
        setDateFormat(DATE_FORMAT);
    }
}
