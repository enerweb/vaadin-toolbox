package za.co.enerweb.toolbox.vaadin;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Joiner;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

/**
 * A informative message panel with a nice icon.
 */
public class InfoComponent extends Panel {

    private static final long serialVersionUID = 1L;

    protected VerticalLayout mainLayout;

    {
        mainLayout = new VerticalLayout();
        super.setContent(mainLayout);
        setVisible(false);
        setSizeUndefined();
        // setWidth("100%");
    }

    /**
     * If infoItems is null or empty, then this layout will be left empty.
     * If there are more than one infoItems then it will be converted into
     * bullet points.
     * @param infoText may be html
     */
    public InfoComponent(String... infoItems) {
        if (infoItems != null && infoItems.length != 0
            && infoItems[0] != null &&
                !StringUtils.strip(infoItems[0]).isEmpty()) {
            String infoText = infoItems[0];
            if (infoItems.length > 1) {
                infoText = "<ul><li>" + Joiner.on("</li><li>").join(infoItems)
                    + "</li></ul>";
            }
            Label label = new Label(infoText, ContentMode.HTML);
            setContent(label);
        }
    }

    public void setContent(Component component) {
        if (mainLayout == null) {
            // gets called by Panel()
            return;
        }
        mainLayout.removeAllComponents();
        // HorizontalLayout layout = new HorizontalLayout();
        // layout.addStyleName(Styles.STYLE_PADDED_CONTENT);
        // layout.setMargin(true);
        // layout.setSpacing(true);
        // super.setContent(layout);

        Embedded image =
            new Embedded(null, new ThemeResource("img/info.png"));
        image.setWidth("18px");
        image.setHeight("18px");
        mainLayout.addComponent(image);
        mainLayout.addComponent(component);
        setVisible(true);
    }

}
