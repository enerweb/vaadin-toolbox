package za.co.enerweb.toolbox.vaadin.messaging;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.google.common.collect.Lists;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;

/**
 * All components added to this that implements {@link MessageListener} will get
 * onMessage events. This follows the publish + subscribe model = observer
 * pattern.
 */
@NoArgsConstructor
@Slf4j
public class MessageBus implements Serializable {

    private static final long serialVersionUID = 1L;

    // allow components to disappear if they are not used any more
    private transient Map<Component, Component> components =
        new WeakHashMap<Component, Component>();

    private transient Map<MessageListener, MessageListener>
        nonComponentMessageListerner =
        new WeakHashMap<MessageListener, MessageListener>();

    public MessageBus(Component component) {
        addComponent(component);
    }

    private Map<Component, Component> getComponentMap() {
        if (components == null) {
            components = new WeakHashMap<Component, Component>();
        }
        return components;
    }

    private Map<MessageListener, MessageListener>
        getNonComponentMessageListernerMap() {
        if (nonComponentMessageListerner == null) {
            nonComponentMessageListerner =
                new WeakHashMap<MessageListener, MessageListener>();
        }
        return nonComponentMessageListerner;
    }

    protected Collection<Component> getComponents() {
        // return a clone of the components so that more can be added
        // during an event
        return new ArrayList<Component>(getComponentMap().keySet());
    }

    /**
     * @param component
     */
    public void addComponent(Component component) {
        getComponentMap().put(component, component);
    }

    public void addNonComponentMessageListerner(
        MessageListener messageListener) {
        if (messageListener instanceof Component) {
            log.warn("Registering a component as a "
                + "non-ComponentMessageListerner: " + messageListener);
        }
        getNonComponentMessageListernerMap()
            .put(messageListener, messageListener);
    }

    /**
     * Rather use fireMessage(Component source, Message message) from outside
     * @param message
     */
    private void fireMessage(Message message) {
        // allow null source only while testing
        if (message.getSource() == null && !MessageBusFactory.isTestMode()) {
            throw new IllegalArgumentException(
                "Message source may not be null");
        }
        log.debug("Sending " + message + "\n  from " + message.getSource());
        fireMessageOnComponents(getComponents().iterator(), message);
        for (MessageListener listener : getNonComponentMessageListernerMap()
            .keySet()) {
            callOnMessage(null, message, listener);
        }
    }

    /**
     * Call this to send a message to all of its listening components.
     * @param source Messages will not be sent back to the source.
     * @param message
     */
    public void fireMessage(Component source, Message message) {
        if (message == null) {
            return;
        }
        message.setSource(source);
        fireMessage(message);
    }

    protected void fireMessageOnComponents(
        Iterator<Component> componentIterator,
        Message message) {
        while (componentIterator.hasNext()) {
            fireMessageOnComponent(componentIterator.next(), message);
        }
    }

    protected void fireMessageOnComponent(
        Component targetComponent,
        final Message message) {
        // don't send messages to the source, but do send it to its
        // children
        if (targetComponent instanceof MessageListener) {
            // maybe this should happen in a background thread
            // sleeping a while between messages
            final MessageListener listener =
                (MessageListener) targetComponent;
            // ScreenMessageQueue.queueMessage(listener, screenMessage
            callOnMessage(targetComponent, message, listener);
        }
        if (targetComponent instanceof ComponentContainer) {
            ComponentContainer cc = (ComponentContainer) targetComponent;

            // clone the iterator because it may get modified
            // when firing a message into it..
            fireMessageOnComponents(
                Lists.newArrayList(cc.iterator()).iterator()
                , message);
        }
    }

    protected static void callOnMessage(Component targetComponent,
        final Message message, final MessageListener listener) {
        if (listener.equals(message.getSource())) {
            // don't send messages to yourself
            return;
        }
        try {
            long startTime = System.currentTimeMillis();
            if (targetComponent == null && listener instanceof Component) {
                targetComponent = (Component) listener;
            }
            log.debug("  to " + listener.getClass());

            // NB. the message sender can require that the message be delivered
            // while he waits, so if Message.synchronous = true then,
            // don't do this..

            // if (listener instanceof AsyncMessageListener) {
            // Thread listenerThread = new Thread(new Runnable() {
            //
            // @Override
            // public void run() {
            // listener.onMessage(message);
            // }
            // });
            // listenerThread.setDaemon(true);
            // listenerThread.start();
            // } else {
            if (targetComponent != null
                && targetComponent.getUI() != null) {
                synchronized (targetComponent.getUI()) {
                    listener.onMessage(message);
                }
            } else {
                // if the component is not attached so it does not really
                // matter that we can't synchronize on the application..
                listener.onMessage(message);
            }
            // }
            long duration = System.currentTimeMillis() - startTime;
            if (duration > 1000) {
                log.warn("Message Bus took more than 1 second :"
                    + listener.getClass() + " " + duration + " ms");
            }

        } catch (Exception e) {
            log.error("An error occured while sending "
                + message + " to " + listener, e);
        }
    }

    public void removeComponent(Component component) {
        components.remove(component);
    }
}
