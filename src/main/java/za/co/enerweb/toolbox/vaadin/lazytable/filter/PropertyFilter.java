package za.co.enerweb.toolbox.vaadin.lazytable.filter;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PropertyFilter extends AFilter {

    private Object propertyId;
    private PropertyFilterOperator operator;
    private Object value;
    private String propertyCaption;

    public PropertyFilter(Object propertyId, Object value) {
        this.propertyId = propertyId;
        this.value = value;
        operator = PropertyFilterOperator.EQUALS;
    }

    public PropertyFilter(Object propertyId, Object value,
        String propertyCaption) {
        this.propertyId = propertyId;
        this.value = value;
        operator = PropertyFilterOperator.EQUALS;
        this.propertyCaption = propertyCaption;
    }

    public PropertyFilter(Object propertyId, PropertyFilterOperator operator,
        Object value) {
        super();
        this.propertyId = propertyId;
        this.operator = operator;
        this.value = value;
    }

    public void acceptVisitor(AFilterVisitor visitor) {
        visitor.visit(this);
    }

    public String getPropertyCaption() {
        return propertyCaption != null ? propertyCaption :
            propertyId.toString();
    }
}
