package za.co.enerweb.toolbox.vaadin.lazytable;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import za.co.enerweb.toolbox.vaadin.lazytable.filter.AFilter;

public class NullLazyDataProducer<T> extends ALazyDataProducer<T> {

    public NullLazyDataProducer(Class<T> dataObjectClass) {
        super(dataObjectClass);
    }

    public int count(AFilter aFilter) {
        return 0;
    }

    public List<T> find(
        final int startIndex,
        final int count, SortSpec sortSpec, AFilter aFilter) {
        return Collections.emptyList();
    }

    public List<PropertyInfo> getPropertyInfos() {
        return Collections.emptyList();
    }

    public Object getPropertyValue(T dataObject,
        Serializable propertyId) {
        throw new UnsupportedOperationException();
    }

    public void setPropertyValue(T dataObject,
        Serializable propertyId, Object value) {
        throw new UnsupportedOperationException();
    }
}
