package za.co.enerweb.toolbox.vaadin.lazytable;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import za.co.enerweb.toolbox.vaadin.ConfirmationDialog;
import za.co.enerweb.toolbox.vaadin.ConfirmationDialog.ConfirmationListener;

import com.vaadin.ui.Component;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormFieldFactory;

public class ObjectEditorDialog extends ConfirmationDialog implements
    ConfirmationListener {

    private final LazyDataItem<Serializable> lazyDataItem;
    private Form form = new Form();
    private final FormFieldFactory formFieldFactory;

    public ObjectEditorDialog(String itemName,
        FormFieldFactory formFieldFactory,
        LazyDataItem<Serializable> beanItem) {
        super(StringUtils.capitalize(itemName) + " editor dialog.");
        this.formFieldFactory = formFieldFactory;
        this.lazyDataItem = beanItem;
        addConfirmationListener(this);
    }

    @Override
    protected Component getConfirmationDialogContent() {
        form.setItemDataSource(lazyDataItem);
        form.setFormFieldFactory(formFieldFactory);
        form.setReadOnly(false);
        return form;
    }

    @Override
    public void onOk() {
        form.commit();
    }

    @Override
    public void onCancel() {
        form.discard();
    }
}
