package za.co.enerweb.toolbox.vaadin.lazytable.filter;

import java.util.ArrayList;
import java.util.List;

public class AndFilters extends AFilter {
    private List<AFilter> filters = new ArrayList<AFilter>();

    public void addProperty(Object propertyId, Object value) {
        add(new PropertyFilter(propertyId, value));
    }

    public void addProperty(Object propertyId, PropertyFilterOperator operator,
        Object value) {
        add(new PropertyFilter(propertyId, operator, value));
    }

    public void addProperty(Object propertyId, Object value,
        String propertyCaption) {
        add(new PropertyFilter(propertyId, value, propertyCaption));
    }

    public void addProperty(Object propertyId, PropertyFilterOperator operator,
        Object value, String propertyCaption) {
        add(new PropertyFilter(propertyId, operator, value,
            propertyCaption));
    }

    public void add(AFilter filter) {
        if (filter instanceof AndFilters) {
            // add the components here, so we don't get
            // unneeded empty brackets ()
            for (AFilter f : ((AndFilters) filter).filters) {
                add(f);
            }
        } else {
            filters.add(filter);
        }
    }

    public void acceptVisitor(AFilterVisitor visitor) {
        if (filters.isEmpty()) {
            return;
        }
        visitor.andStart(this);
        boolean first = true;
        for (AFilter f : filters) {
            if (first) {
                first = false;
            } else {
                visitor.and(this);
            }
            f.acceptVisitor(visitor);
        }
        visitor.andEnd(this);
    }

    public int size() {
        return filters.size();
    }

    public boolean isEmpty() {
        return filters.isEmpty();
    }

    public void clear() {
        filters.clear();
    }
}
