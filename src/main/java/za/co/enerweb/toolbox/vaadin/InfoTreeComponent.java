package za.co.enerweb.toolbox.vaadin;

import com.vaadin.ui.Tree;

/**
 * A informative message panel with a nice icon.
 * This class displays a help tree
 */
public class InfoTreeComponent extends InfoComponent {

    private static final long serialVersionUID = 1L;
    private Tree tree;

    public InfoTreeComponent() {
        tree = new Tree();
        tree.setSelectable(false);
        setContent(tree);
    }

    public String addTreeNode(String caption, boolean allowChildren) {
        tree.addItem(caption);
        tree.setChildrenAllowed(caption, allowChildren);
        return caption;
    }

    public String addTreeNode(String caption, String parentCaption,
            boolean allowChildren) {
        addTreeNode(caption, allowChildren);
        tree.setParent(caption, parentCaption);
        return caption;
    }

}
