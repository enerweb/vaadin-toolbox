package za.co.enerweb.toolbox.vaadin.messaging;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.vaadin.ui.Component;

/**
 * You should extend this class for any nontrivial messages
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    private Component source;
    private String messageId;
    /**
     * The message creator can set this to true if he needs/relies on the
     * message being delivered while he waits
     */
    private boolean synchronous = false;

    public Message(Component source, String messageId) {
        super();
        this.source = source;
        this.messageId = messageId;
    }

    public void setSource(Component source){
        this.source = source;
        this.messageId = getClass().getName();
    }
}
