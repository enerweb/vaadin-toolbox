package za.co.enerweb.toolbox.vaadin.lazytable;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import za.co.enerweb.toolbox.vaadin.lazytable.filter.AFilter;
import za.co.enerweb.toolbox.vaadin.lazytable.filter.NullFilter;

import com.vaadin.data.Property;
import com.vaadin.ui.Component;

/**
 * T is the data object class type (represents a row).
 */
public abstract class ALazyDataProducer<T> {

    @Getter
    private final Class<T> dataObjectClass;
    @Getter
    @Setter
    private AFilter filter = new NullFilter();
    @Getter
    @Setter
    private SortSpec defaultSortSpec = new SortSpec();

    public ALazyDataProducer(Class<T> dataObjectClass) {
        this.dataObjectClass = dataObjectClass;
    }

    // XXX: since we know the filter now, we can probably get rid
    // of passing it as a param..
    public abstract int count(AFilter aFilter);

    public abstract List<T> find(
        final int startIndex,
        final int count, SortSpec sortSpec, AFilter aFilter);

    public abstract List<PropertyInfo> getPropertyInfos();

    public abstract Object getPropertyValue(T dataObject,
        Serializable propertyId);

    public abstract void setPropertyValue(T dataObject,
        Serializable propertyId, Object value);

    /**
     * This is not guaranteed to be called for all new instances,
     * if the is for an abstract type, then components can make up their
     * own default new instances..
     */
    public T newInstance() {
        throw new UnsupportedOperationException();
    }

    // public final void saveSerializableItems(final List<Serializable>
    // addedItems,
    // final List<Serializable> modifiedItems,
    // final List<Serializable> removedItems) {
    // throw new UnsupportedOperationException();
    // }

    public void saveItems(final List<T> addedItems,
        final List<T> modifiedItems,
        final List<T> removedItems) {
        throw new UnsupportedOperationException();
    }

    /**
     * @return null, if this property should use the default
     */
    public Object getPropertyDisplayValue(Object rowId, Object colId,
        Property property) {
        if (property.getValue() instanceof Component) {
            return property.getValue();
        }
        return null;
    }

    /**
     * If the producer caches anything, it should reload it here please.
     */
    public void reload() {
    }
}
