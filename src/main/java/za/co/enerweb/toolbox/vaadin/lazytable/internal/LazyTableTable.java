package za.co.enerweb.toolbox.vaadin.lazytable.internal;

import lombok.val;
import za.co.enerweb.toolbox.vaadin.lazytable.ALazyDataProducer;

import com.vaadin.data.Property;
import com.vaadin.ui.Table;

/**
 * T is the data object class type (represents a row).
 */
public class LazyTableTable<T> extends Table {
    private final ALazyDataProducer<T> aLazyDataProducer;

    public LazyTableTable(ALazyDataProducer<T> aLazyDataProducer) {
        this.aLazyDataProducer = aLazyDataProducer;
    }

    protected Object getPropertyValue(Object rowId, Object colId,
        Property property) {
        val ret =
            aLazyDataProducer.getPropertyDisplayValue(rowId, colId, property);
        if (ret != null) {
            return ret;
        }
        return super.getPropertyValue(rowId, colId, property);
    }
}
