package za.co.enerweb.toolbox.vaadin.lazytable.filter;

import lombok.Getter;

public enum PropertyFilterOperator {
    EQUALS("="), NOT_EQUALS("<>"), LIKE("like"), GREATER(">"),
    GREATER_OR_EQUALS(">="),
    LESS("<"), LESS_OR_EQUALS("<=");

    @Getter
    private String sql;

    private PropertyFilterOperator(String sql) {
        this.sql = sql;
    }
}
