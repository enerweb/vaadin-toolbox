package za.co.enerweb.toolbox.vaadin.highchart;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

import com.vaadin.ui.Component;

@Data
@AllArgsConstructor
public class TimelineZoomEvent {
    Component source;
    Date minDate;
    Date maxDate;
}
