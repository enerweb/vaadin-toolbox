package za.co.enerweb.toolbox.vaadin.lazytable;

import java.util.List;
import java.util.Map;

import lombok.Getter;

import com.vaadin.data.Property;
import com.vaadin.data.util.PropertysetItem;

/*
 * Heavily adapted from com.vaadin.data.util.BeanItem
 * FIXME: this does not seem to work properly yet, I could not get my stuff
 * to save with this so reverted back to LazyDataItem for now
 */
public class LazyDataItem<T> extends PropertysetItem {

    @Getter
    private final T dataObject;

    // public LazyDataItem(T bean, boolean readOnly) {
    // this(bean, (Map<String, String>) null, readOnly);
    // }
    //
    public LazyDataItem(T dataObject,
        ALazyDataProducer<T> aLazyDataProducer,
        List<PropertyInfo> propertyInfos,
        boolean readOnly) {
        this(dataObject, aLazyDataProducer, propertyInfos, null, readOnly);
    }


    public LazyDataItem(T dataObject,
        ALazyDataProducer<T> aLazyDataProducer,
        List<PropertyInfo> propertyInfos,
        Map<Object, String> colDisplayProperties, boolean readOnly) {

        this.dataObject = dataObject;

        for (PropertyInfo pi : propertyInfos) {
            String displayProperty = null;
            if (colDisplayProperties != null) {
                displayProperty = colDisplayProperties.get(pi.id());
            }
            Property property = new PrettyProperty<T>(aLazyDataProducer,
                pi, dataObject,
                displayProperty,
                // readonly if we display something else
                colDisplayProperties != null
                );
            addItemProperty(pi.id(), property);
        }
    }

}
