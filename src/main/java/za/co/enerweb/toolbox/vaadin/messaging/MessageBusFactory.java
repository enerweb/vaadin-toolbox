package za.co.enerweb.toolbox.vaadin.messaging;

import java.util.Map;
import java.util.WeakHashMap;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

/**
 * Keeps one messagebus per application.
 */
@Slf4j
public abstract class MessageBusFactory {

    private static Map<VaadinSession, MessageBus> activeBusses =
        new WeakHashMap<VaadinSession, MessageBus>();

    @Setter
    @Getter
    private static boolean testMode = false;

    public synchronized static MessageBus getInstance(UI ui) {
        VaadinSession session = null;
        if (ui == null) {
            ui = UI.getCurrent();
        }
        // if (!testMode) {
        // throw new IllegalArgumentException("Can only obtain a message bus "
        // + "for an attached component (which has a non-null ui)"
        // );
        // }
        // } else {
            session = ui.getSession();
        // }
        return getInstance(session);
    }

    private static MessageBus getInstance(VaadinSession session) {
        MessageBus messageBus = activeBusses.get(session);
        if (messageBus == null) {
            messageBus = new MessageBus();
            activeBusses.put(session, messageBus);
        }
        return messageBus;
    }

    public static MessageBus getInstance(Component component) {
        if (component == null) {
            return getTestInstance();
        }
        return getInstance(component.getUI());
    }

    public static MessageBus getTestInstance() {
        return getInstance((UI) null);
    }

    public static void fireMessage(final Component source,
        final Message message) {
        if (source != null &&
            source.getUI() == null && !message.isSynchronous()) {
            Thread t = new Thread() {
                public void run() {
                    // wait 200 milli seconds to see if it get's attached
                    // shortly

                    int sleeptime = 50;
                    int retries = 200 / sleeptime;
                    for (int i = 0; source.getUI() == null
                        && i < retries; i++) {
                        try {
                            log.debug("Delaying message " + sleeptime
                                + "ms fire until source " +
                                "component is attached. " + i);
                            Thread.sleep(sleeptime);
                        } catch (InterruptedException e) {
                            log.warn("Interrupted while waiting for component" +
                                " to be attached, so it can fire a message."
                                , e);
                            break;
                        }
                    }
                    getInstance(source).fireMessage(source, message);
                }
            };
            t.setDaemon(true);
            t.start();
        } else {
            getInstance(source).fireMessage(source, message);
        }
    }

    /**
     * Components that want so receive the current status of others can call:
     * <pre>
     * MessageBusFactory.requestContextUpdate(this, this);
     * </pre>
     * Components that want to send their current status on request must
     * handle the following message:
     * <pre>
     * if (message instanceof MRequestContextUpdate) {
     *     ((MRequestContextUpdate) message).updateContext(this, MSomeMessage);
     * }
     * </pre>
     * @param source
     * @param requestor
     */
    public static void requestContextUpdate(Component source,
        MessageListener requestor) {
        fireMessage(source,
            new MRequestContextUpdate(requestor));
    }

    public static void sendMessageDirectly(Component source, Message message,
        MessageListener listener) {
        if (message == null) {
            return;
        }
        message.setSource(source);
        MessageBus.callOnMessage(null, message, listener);
    }

    public static void registerComponent(Component component) {
        registerComponent(component.getUI(), component);
    }
    public static void unregisterComponent(Component component) {
        registerComponent(component.getUI(), component);
    }

    public static void unregisterComponent(UI ui,
        Component component) {
        getInstance(ui).removeComponent(component);
    }

    public static void registerComponent(UI ui,
        Component component) {
        getInstance(ui).addComponent(component);
    }
}
