package za.co.enerweb.toolbox.vaadin.highchart;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

import org.junit.Test;

import com.invient.vaadin.charts.Color.RGB;
@Slf4j
public class ColorGeneratorTest {

    private ColorGenerator cg = new ColorGenerator();

    @Test
    public void test() {
        // check that we can get lots of unique colours
        Set<String> uniqueColors = new HashSet<String>();
        for (int i = 1; i < 100; i++) {
            RGB color = cg.getColor(i);
            log.debug(i + " got color: " + color);
            assertTrue(uniqueColors.add(color.toString()));
        }
    }

}
